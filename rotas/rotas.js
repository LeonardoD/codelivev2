module.exports = function(app) {
    var upload = app.get('upload');
    var usuario = app.api.usuario;
    app.post('/login', usuario.login);
    app.get('/logoff', usuario.logoff);
    app.get('/get-usuario-logado', usuario.getUsuarioLogado);
    app.post('/usuario', usuario.setUsuario);
    app.get('/usuario', usuario.getUsuarios);
    app.get('/usuario/:id', usuario.getUsuario);
    app.put('/usuario/:id', usuario.updateUsuario);
    app.delete('/usuario/:id', usuario.deleteUsuario);
    app.put('/update-senha-usuario/:id', usuario.updateSenhaUsuario);
    app.post('/upload-imagem', upload.single('file'), usuario.uploadImagem);
    app.get('/get-avatares', usuario.getAvatar);
    app.get('/get-conquistas-usuario/:id', usuario.getConquistaUsuario);
    app.post('/set-conquista-usuario', usuario.setConquistaUsuario);
    app.post('/update-credito-usuario', usuario.updateCreditoUsuario);
    app.post('/responder-treino', usuario.setRespostaTreino);
    app.post('/send-email', usuario.sendEmail);

    var desafio = app.api.desafio;
    app.get('/desafio', desafio.getDesafios);
    app.post('/desafio', desafio.setDesafio);
    app.get('/desafio/:id', desafio.getDesafio);
    app.put('/desafio/:id', desafio.updateDesafio);
    app.get('/desafios-por-usuario/:id', desafio.getDesafiosPorUsuario);
    app.put('/ativar-desafio/:id', desafio.ativarDesafio);
    app.put('/desativar-desafio/:id', desafio.desativarDesafio);
    app.post('/comprar-desafio', desafio.comprarDesafio);
    app.post('/responder-desafio', desafio.setRespostaDesafio);
    app.get('/historico-desafio-usuario/:id', desafio.getHistoricoDesafioUsuario);
    app.get('/historico-desafio/:id', desafio.getHistoricoDesafio);
    app.get('/verifica-historico-desafio/:id', desafio.verificaHistoricoDesafio);
    app.post('/verifica-desafio-vencido', desafio.verificaDesafioVencido);
    app.get('/get-desafios-comprados/:id', desafio.getDesafiosComprados);
}