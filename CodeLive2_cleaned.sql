-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 03-Dez-2016 às 15:25
-- Versão do servidor: 10.1.10-MariaDB
-- PHP Version: 7.0.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `CodeLive2`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `AvaliacaoDesafio`
--

CREATE TABLE `AvaliacaoDesafio` (
  `Id` int(11) NOT NULL,
  `Comentario` text,
  `Nota` int(11) DEFAULT NULL,
  `DataHora` datetime DEFAULT NULL,
  `IdUsuario` int(11) NOT NULL,
  `IdDesafio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Conquista`
--

CREATE TABLE `Conquista` (
  `Id` int(11) NOT NULL,
  `Nome` varchar(50) DEFAULT NULL,
  `IdUsuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Desafio`
--

CREATE TABLE `Desafio` (
  `Id` int(11) NOT NULL,
  `Titulo` varchar(45) DEFAULT NULL,
  `Tags` text,
  `Tempo` time DEFAULT NULL,
  `Descricao` varchar(255) DEFAULT NULL,
  `Enunciado` text,
  `PreCodigo` text,
  `Publicado` tinyint(1) DEFAULT NULL COMMENT 'Campo quando "true", indica que o Desafio está publicado e pode ser visualizado por todos os usuários.',
  `Autor` int(11) NOT NULL COMMENT 'Usuario que criou o Desafio. Importante lembrar que o criador do Desafio não pode participar do mesmo.',
  `Dificuldade` int(11) DEFAULT NULL COMMENT 'Este campo indica que apenas um usuário com determinado NivelDf Poderá participar do Desafio. e.g: NivelDificuldade = 2, significa que os usuários NivelDf 1(novatos) não Poderão participar desse Desafio.',
  `DataCadastro` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `EntradasDesafio`
--

CREATE TABLE `EntradasDesafio` (
  `Id` int(11) NOT NULL,
  `Valor` text,
  `IdDesafio` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `HistoricoDesafio`
--

CREATE TABLE `HistoricoDesafio` (
  `Id` int(11) NOT NULL,
  `IdResposta` int(11) NOT NULL,
  `DesafioVencido` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Imagem`
--

CREATE TABLE `Imagem` (
  `Id` int(11) NOT NULL,
  `Endereco` varchar(255) DEFAULT NULL,
  `Descricao` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `Imagem`
--

INSERT INTO `Imagem` (`Id`, `Endereco`, `Descricao`) VALUES
(1, '\\assets\\img\\personagens\\default.png', 'default'),
(2, '\\assets\\img\\personagens\\anakin.jpg', 'avatar'),
(3, '\\assets\\img\\personagens\\ioda.jpg', 'avatar'),
(4, '\\assets\\img\\personagens\\obi-wan.jpg', 'avatar'),
(5, '\\assets\\img\\personagens\\padawan.jpg', 'avatar'),
(6, '\\assets\\img\\personagens\\palpatine.jpg', 'avatar'),
(7, 'assets/img/personagens/darth-vader.jpg', 'avatar'),
(8, '\\assets\\img\\fotosUpload\\4565445-goku_by_maffo1989-d4vxux4-1475153991992.png', 'avatar-usuario');

-- --------------------------------------------------------

--
-- Estrutura da tabela `Resposta`
--

CREATE TABLE `Resposta` (
  `Id` int(11) NOT NULL,
  `Resposta` text COLLATE utf8_unicode_ci,
  `IdDesafio` int(11) DEFAULT NULL,
  `IdUsuario` int(11) DEFAULT NULL,
  `DataResposta` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `RespostaTreino`
--

CREATE TABLE `RespostaTreino` (
  `Id` int(11) NOT NULL,
  `IdUsuario` int(11) NOT NULL,
  `IdDesafio` int(11) NOT NULL,
  `Resposta` text COLLATE utf8_unicode_ci NOT NULL,
  `DataResposta` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `SaidasDesafio`
--

CREATE TABLE `SaidasDesafio` (
  `Id` int(11) NOT NULL,
  `Valor` text,
  `IdDesafio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Usuario`
--

CREATE TABLE `Usuario` (
  `Id` int(11) NOT NULL,
  `Nome` varchar(150) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `Senha` varchar(255) DEFAULT NULL,
  `Nivel` int(11) DEFAULT NULL COMMENT 'é uma pontuação numerica que indica o quão avançado está o usuário no jogo dentro de uma determinado classe.',
  `IdImagem` int(11) DEFAULT NULL,
  `NivelDf` bigint(20) DEFAULT NULL COMMENT 'e.g: novato, cavaleiro jedi, padawan, etc.',
  `Credito` int(11) DEFAULT NULL,
  `NomeExibicao` varchar(100) DEFAULT NULL,
  `Classe` varchar(50) DEFAULT NULL COMMENT 'Classe que o usuário de acordo com o NíveDf que ele possui. e.g: Novato, Padawan, Cavaleiro Jedi ou Mestre Jedi.',
  `DataCadastro` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `Usuario`
--

INSERT INTO `Usuario` (`Id`, `Nome`, `Email`, `Senha`, `Nivel`, `IdImagem`, `NivelDf`, `Credito`, `NomeExibicao`, `Classe`, `DataCadastro`) VALUES
(1, 'Jesiel S. Padilha', 'jesielpadilha.ti@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 1, 8, 30900, 50445, 'jesiel.padilha', 'Mestre do conselho', '2015-09-30 17:33:54');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `AvaliacaoDesafio`
--
ALTER TABLE `AvaliacaoDesafio`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Id_UNIQUE` (`Id`),
  ADD KEY `fk_Comentario_Usuario1_idx` (`IdUsuario`),
  ADD KEY `fk_Comentario_Desafio1_idx` (`IdDesafio`);

--
-- Indexes for table `Conquista`
--
ALTER TABLE `Conquista`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Id_UNIQUE` (`Id`),
  ADD KEY `fk_Conquista_Usuario1_idx` (`IdUsuario`);

--
-- Indexes for table `Desafio`
--
ALTER TABLE `Desafio`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `fk_Desafio_Usuario1_idx` (`Autor`);

--
-- Indexes for table `EntradasDesafio`
--
ALTER TABLE `EntradasDesafio`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IdDesafio` (`IdDesafio`);

--
-- Indexes for table `HistoricoDesafio`
--
ALTER TABLE `HistoricoDesafio`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Id_UNIQUE` (`Id`),
  ADD KEY `fk_HistorioDesafio_Usuario1_idx` (`IdResposta`);

--
-- Indexes for table `Imagem`
--
ALTER TABLE `Imagem`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `idImagem_UNIQUE` (`Id`);

--
-- Indexes for table `Resposta`
--
ALTER TABLE `Resposta`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IdDesafio` (`IdDesafio`),
  ADD KEY `IdUsuario` (`IdUsuario`);

--
-- Indexes for table `RespostaTreino`
--
ALTER TABLE `RespostaTreino`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IdUsuario` (`IdUsuario`),
  ADD KEY `IdDesafio` (`IdDesafio`);

--
-- Indexes for table `SaidasDesafio`
--
ALTER TABLE `SaidasDesafio`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Id_UNIQUE` (`Id`),
  ADD KEY `fk_SaidasDesafio_Desafio1_idx` (`IdDesafio`);

--
-- Indexes for table `Usuario`
--
ALTER TABLE `Usuario`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Id_UNIQUE` (`Id`),
  ADD KEY `fk_Usuario_Imagem1_idx` (`IdImagem`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Conquista`
--
ALTER TABLE `Conquista`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Desafio`
--
ALTER TABLE `Desafio`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `EntradasDesafio`
--
ALTER TABLE `EntradasDesafio`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `HistoricoDesafio`
--
ALTER TABLE `HistoricoDesafio`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Imagem`
--
ALTER TABLE `Imagem`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `Resposta`
--
ALTER TABLE `Resposta`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `RespostaTreino`
--
ALTER TABLE `RespostaTreino`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `SaidasDesafio`
--
ALTER TABLE `SaidasDesafio`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Usuario`
--
ALTER TABLE `Usuario`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `AvaliacaoDesafio`
--
ALTER TABLE `AvaliacaoDesafio`
  ADD CONSTRAINT `fk_Comentario_Desafio1` FOREIGN KEY (`IdDesafio`) REFERENCES `Desafio` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_Comentario_Usuario1` FOREIGN KEY (`IdUsuario`) REFERENCES `Usuario` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `Conquista`
--
ALTER TABLE `Conquista`
  ADD CONSTRAINT `fk_Conquista_Usuario1` FOREIGN KEY (`IdUsuario`) REFERENCES `Usuario` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `Desafio`
--
ALTER TABLE `Desafio`
  ADD CONSTRAINT `fk_Desafio_Usuario1` FOREIGN KEY (`Autor`) REFERENCES `Usuario` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `EntradasDesafio`
--
ALTER TABLE `EntradasDesafio`
  ADD CONSTRAINT `EntradasDesafio_ibfk_1` FOREIGN KEY (`IdDesafio`) REFERENCES `Desafio` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `HistoricoDesafio`
--
ALTER TABLE `HistoricoDesafio`
  ADD CONSTRAINT `historicodesafio_ibfk_1` FOREIGN KEY (`IdResposta`) REFERENCES `Resposta` (`Id`);

--
-- Limitadores para a tabela `Resposta`
--
ALTER TABLE `Resposta`
  ADD CONSTRAINT `Resposta_ibfk_1` FOREIGN KEY (`IdDesafio`) REFERENCES `Desafio` (`Id`),
  ADD CONSTRAINT `Resposta_ibfk_2` FOREIGN KEY (`IdUsuario`) REFERENCES `Usuario` (`Id`);

--
-- Limitadores para a tabela `RespostaTreino`
--
ALTER TABLE `RespostaTreino`
  ADD CONSTRAINT `respostatreino_ibfk_1` FOREIGN KEY (`IdUsuario`) REFERENCES `Usuario` (`Id`),
  ADD CONSTRAINT `respostatreino_ibfk_2` FOREIGN KEY (`IdDesafio`) REFERENCES `Desafio` (`Id`);

--
-- Limitadores para a tabela `SaidasDesafio`
--
ALTER TABLE `SaidasDesafio`
  ADD CONSTRAINT `fk_SaidasDesafio_Desafio1` FOREIGN KEY (`IdDesafio`) REFERENCES `Desafio` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `Usuario`
--
ALTER TABLE `Usuario`
  ADD CONSTRAINT `Usuario_ibfk_1` FOREIGN KEY (`IdImagem`) REFERENCES `Imagem` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
