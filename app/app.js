var app = angular.module("CodeLive", ['ngMessages', 'ngRoute', 'ui.bootstrap', 'timer', 'ngStorage', 'ui.codemirror', 'summernote', 'angular-loading-bar', 'ngTagsInput']);
var socket = io();
var host = 'http://localhost:3000/';
app.config(function($routeProvider) {
    $routeProvider
        .when("/adm", {
            templateUrl: "adm.html",
            controller: "UsuarioCtrl",
        })
        .when("/perfil", {
            templateUrl: "views/usuario/perfil.html",
            controller: "UsuarioCtrl",
        })
        .when("/editar-perfil", {
            templateUrl: "views/usuario/editarPerfil.html",
            controller: "UsuarioCtrl",
        })
        .when("/desafios", {
            templateUrl: "views/desafio/desafios.html",
            controller: "DesafioCtrl",
        })
        .when("/novo-desafio", {
            templateUrl: "views/desafio/cadastrarDesafio.html",
            controller: "CriarDesafioCtrl"
        })
        .when("/editar-desafio/:id", {
            templateUrl: "views/desafio/editarDesafio.html",
            controller: "EditarDesafioCtrl",
            resolve: {
                desafio: function($rootScope, $route) {
                    return $rootScope.idDesafio = $route.current.params.id;
                }
            }
        })
        .when("/login", {
            templateUrl: "views/login.html",
            controller: "LoginCtrl"
        })
        .when("/cadastro", {
            templateUrl: "views/usuario/cadastroUsuario.html",
            controller: "CadastroUsuarioCtrl"
        })
        .when("/gerenciar-meus-desafios", {
            templateUrl: "views/usuario/gerenciarMeusDesafios.html",
            controller: "GerenciarMeusDesafiosCtrl"
        })
        .when("/responder-desafio/:id", {
            templateUrl: "views/desafio/responderDesafio.html",
            controller: "ResponderDesafioCtrl",
            resolve: {
                desafio: function($rootScope, $route) {
                    return $rootScope.idDesafio = $route.current.params.id;
                }
            }
        })
        .when("/perfil-autor/:id", {
            templateUrl: "views/usuario/perfilAutor.html",
            controller: "AutorCtrl",
            resolve: {
                desafio: function($rootScope, $route) {
                    return $rootScope.idUsuario = $route.current.params.id;
                }
            }
        })
        .when("/desafios-comprados", {
            templateUrl: "views/usuario/desafiosComprados.html",
            controller: "DesafiosCompradosCtrl",
        })
        .when("/detalhes-desafio-comprado/:id", {
            templateUrl: "views/usuario/detalhesDesafioComprado.html",
            controller: "DesafiosCompradosCtrl",
            resolve: {
                desafio: function($rootScope, $route) {
                    return $rootScope.idDesafio = $route.current.params.id;
                }
            }
        })
        .when("/treino-com-o-desafio/:id", {
            templateUrl: "views/usuario/treino.html",
            controller: "TreinoCtrl",
            resolve: {
                desafio: function($rootScope, $route, $localStorage) {
                    return $rootScope.idDesafio = $route.current.params.id;
                }
            }
        });
    $routeProvider.otherwise({ redirectTo: "/login" });
});

app.directive('fileModel', ['$parse', function($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function() {
                scope.$apply(function() {
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);

app.service('usuarioLogado', ['$localStorage', '$rootScope', '$http', function($localStorage, $rootScope, $http) {
    this.verificaUsuarioLogado = function() {
        $http.get(host + 'get-usuario-logado').then(
            function sucess(res) {
                if ($localStorage.usuarioLogado != null && res.data != null && res.data != '') {
                    localStorage.setItem('logged', true);
                    $rootScope.usuarioLogado = $localStorage.usuarioLogado;
                } else {
                    localStorage.setItem('logged', false);
                    location.assign("index.html#!/login");
                }
            },
            function error(err) {
                notification('erro', 'FALHA', 'Falha ao obter o usuário logado!');
                localStorage.setItem('logged', false);
                location.assign("index.html#!/login");
            }
        );
    }
    this.getInformacoes = function() {
        $http.get(host + 'get-usuario-logado').then(
            function success(res) {
                $rootScope.usuarioLogado = res.data;
                $localStorage.usuarioLogado = res.data;
            },
            function error() {
                notification('erro', 'falha', 'Falha ao atualizar o usuário logado ');
            }
        );
    }
}]);

app.service('fileUpload', ['$http', function($http, $rootScope) {
    this.uploadFileToUrl = function(file, uploadUrl) {
        var deferred = jQuery.Deferred();
        var fd = new FormData();
        fd.append('file', file);
        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: { 'Content-Type': undefined }
        }).then(
            function success(res) {
                deferred.resolve(res.data);
            },
            function error() {
                deferred.reject(false);
                notification('erro', 'Falha ao fazer o upload do arquivo!');
            });
        return deferred.promise();
    }
}]);

app.service('validaDesafio', function() {
    //Valida se um usuário pode criar ou não um desafio
    this.validaCriacaoDesafio = function(dificuldade, usuario) {
        if (dificuldade == 1) {
            if (usuario.Classe == "Padawan" || usuario.Classe == "Cavaleiro Jedi" || usuario.Classe == "Mestre Jedi" || usuario.Classe == 'Mestre do conselho' && usuario.Credito >= 20) {
                return true;
            } else {
                return false;
            }
        }
        if (dificuldade == 2) {
            if (usuario.Classe == "Cavaleiro Jedi" || usuario.Classe == "Mestre Jedi" || usuario.Classe == 'Mestre do conselho' && usuario.Credito >= 50) {
                return true;
            } else {
                return false;
            }
        }
        if (dificuldade == 3) {
            if (usuario.Classe == "Cavaleiro Jedi" || usuario.Classe == "Mestre Jedi" || usuario.Classe == 'Mestre do conselho' && usuario.Credito >= 100) {
                return true;
            } else {
                return false;
            }
        }
        if (dificuldade == 4) {
            if (usuario.Classe == "Mestre Jedi" || usuario.Classe == 'Mestre do conselho' && usuario.Credito >= 200) {
                return true;
            } else {
                return false;
            }
        }
    }

    //Validade se um usuário pode comprar ou não um desafio
    this.validaCompraDesafio = function(dificuldade, usuario) {
        if (dificuldade == 1) {
            if (usuario.Classe == "Novato" || usuario.Classe == "Padawan" || usuario.Classe == "Jedi" || usuario.Classe == "Mestre Jedi" || usuario.Classe == 'Mestre do conselho' && usuario.Credito >= 5) {
                return true;
            } else {
                return false;
            }
        }
        if (dificuldade == 2) {
            if (usuario.Classe == "Novato" && usuario.Nivel >= 3 && usuario.Credito >= 15) {
                return true;
            } else {
                if (usuario.Classe == "Padawan" || usuario.Classe == "Jedi" || usuario.Classe == "Mestre Jedi" || usuario.Classe == 'Mestre do conselho' && usuario.Credito >= 15) {
                    return true;
                } else {
                    return false;
                }
            }
        }
        if (dificuldade == 3) {
            if (usuario.Classe == "Padawan" || usuario.Classe == "Jedi" || usuario.Classe == "Mestre Jedi" || usuario.Classe == 'Mestre do conselho' && usuario.Credito >= 25) {
                return true;
            } else {
                return false;
            }
        }
        if (dificuldade == 4) {
            if (usuario.Classe == "Jedi" || usuario.Classe == "Mestre Jedi" || usuario.Classe == 'Mestre do conselho' && usuario.Credito >= 50) {
                return true;
            } else {
                return false;
            }
        }
    }
});

function organizaTags(data) {
    var tags = [];
    var aux = [];
    var qt = data.Tags.indexOf(",") + 1;
    if (qt > 0) {
        for (var i = 0; i < qt; i++) {
            aux.push(data.Tags.split(",")[i]);
            if (aux[i] != undefined) {
                tags.push(data.Tags.split(",")[i]);
            }
        }
    } else {
        tags.push(data.Tags);
    }
    return tags;
}

app.config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
    cfpLoadingBarProvider.includeSpinner = false;
}]);

ion.sound({
    sounds: [{
        name: 'what-another-message'
    }],
    volume: 0.3,
    path: "assets/",
    preload: true
});