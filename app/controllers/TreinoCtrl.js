app.controller('TreinoCtrl', function ($scope, $rootScope, $http, $localStorage, $sce, $location) {
    var saida = "";
    $scope.resposta;
    $scope.tema = "solarized dark"
    $scope.enunciado = true;
    $scope.saida = false;
    $scope.desafio = $rootScope.responderDesafio;
    $("#temasEditor").html(
        '<option>default </option>'
        + '<option>ambiance</option>'
        + '<option>cobalt</option>'
        + '<option>dracula</option>'
        + '<option>eclipse</option>'
        + '<option>material</option>'
        + '<option>mbo</option>'
        + '<option>mdn-like</option>'
        + '<option>neat</option>'
        + '<option>rubyblue</option>'
        + '<option>solarized dark</option>'
        + '<option>twilight</option>'
        + '<option>zenburn</option>'
    );

    $scope.renderHtml = function (code) {
        return $sce.trustAsHtml(code);
    };

    // $('#modalInfoDesafio').modal('show'); mostrar modal de dicas

    var getDesafio = function () {
        var def = jQuery.Deferred();
        $http.get(host + 'desafio/' + $rootScope.idDesafio).then(
            function success(res) {
                def.resolve(res.data[0]);
            }, function error(err) {
                def.reject(err);
                notification('erro', 'FALHA', 'Falha ao obter o desafio!');
            }
        );
        return def.promise();
    }

    function loadDesafio() {
        getDesafio().then(function (data) {
            $scope.desafio = data;
            $scope.desafio.Config = tipoDesafio(data.Dificuldade);
            $scope.desafio.Comprador = $localStorage.usuarioLogado;
            if ($scope.desafio.PreCodigo != null) {
                if ($localStorage.codigoEditorTreino != $scope.desafio.PreCodigo && $localStorage.codigoEditorTreino != '') {
                    $scope.resposta = $localStorage.codigoEditorTreino;
                } else {
                    $scope.resposta = $scope.desafio.PreCodigo;
                    $localStorage.codigoEditorTreino = $scope.desafio.PreCodigo;
                }
            }
            $http.post(host + 'update-credito-usuario', $localStorage.usuarioLogado).then(
                function success(res) {
                    $localStorage.usuarioLogado = res.data;
                    $rootScope.usuarioLogado = res.data;
                }, function error(err) {
                    notification('erro', 'FALHA', 'Falha ao atualizar os créditos do usuário!');
                }
            );
        });
    }
    loadDesafio();

    $scope.salvarCodigo = function (desafio) {
        if ($scope.resposta != undefined && $scope.resposta != "") {
            var resposta = {
                IdUsuario: desafio.Comprador.Id,
                IdDesafio: desafio.Id,
                Resposta: $scope.resposta
            }
            $http.post(host + 'responder-treino', resposta).then(
                function success(res) {
                    notification('sucesso', 'SUCESSO', 'Desafio salvo com sucesso!');
                    $location.path('/desafios-comprados');
                }, function error(err) {
                    notification("erro", "FALHA", "Falha ao salvar o código!");
                }
            );
        } else {
            notification("aviso", "ATENÇÃO", "Digite seu código!");
        }
    }

    $('#editor').keyup(function () {
        $localStorage.codigoEditorTreino = $scope.resposta;
    });

    if ($localStorage.tema == null) {
        $localStorage.tema = { nome: 'default', index: 0 };
    } else {
        $('#temasEditor option:eq(' + $localStorage.tema.index + ')').attr('selected', 'selected');
    }

    $scope.editorOptions = {
        lineNumbers: true,
        styleActiveLine: true,
        matchBrackets: true,
        indentUnit: 4,
        autoMatchParens: true,
        parserConfig: { 'pythonVersion': 2, 'strictErrors': true },
        theme: $localStorage.tema.nome,
        readOnly: false
    };

    $("#temasEditor").change(function () {
        $localStorage.tema = { nome: $("#temasEditor").val(), index: $("#temasEditor")[0].selectedIndex };
        location.reload();
    });

    //***************** Execução do código
    var mypre = document.getElementById("output");
    var outf = function (text) {
        mypre.innerHTML = mypre.innerHTML + text;
    }

    var builtinRead = function (x) {
        if (Sk.builtinFiles === undefined || Sk.builtinFiles["files"][x] === undefined)
            throw "File not found: '" + x + "'";
        return Sk.builtinFiles["files"][x];
    }

    $scope.executarCodigo = function () {
        if ($scope.resposta != undefined && $scope.resposta != "") {
            var prog = $scope.resposta;
            mypre.innerHTML = '';
            Sk.pre = "output";
            Sk.configure({ output: outf, read: builtinRead });
            (Sk.TurtleGraphics || (Sk.TurtleGraphics = {})).target = 'mycanvas';
            var myPromise = Sk.misceval.asyncToPromise(function () {
                return Sk.importMainWithBody("<stdin>", false, prog, true);
            });
            myPromise.then(function (mod) {
                console.log("sucesso");
            }, function (err) {
                $("#output").html('<span style="color: red">' + err.toString() + '</span>');
            });
        } else {
            notification("aviso", "ATENÇÃO", "Digite seu código!");
        }
    }

    var getSaidaExecucao = function () {
        var saidas = $(mypre)[0].outerHTML.replace('<pre id="output" ng-show="saida" class="ng-hide">', '').replace('</pre>', '').split('\n');
        saidas = saidas.splice(0, saidas.length - 1);
        if (String(saidas).includes('<pre')) {
            saidas = String(saidas).replace('<pre id="output" ng-show="saida" class="">', '').split(',');
        }
        return saidas;
    }

});