app.controller("ResponderDesafioCtrl", function($rootScope, $modal, $scope, $http, $localStorage, $sce, $location) {
    var saida = "";
    $scope.resposta;
    $scope.tempoDesafio = 0;
    $scope.tema = "solarized dark"
    $scope.enunciado = true;
    $scope.saida = false;
    $scope.desafio = $rootScope.responderDesafio;
    $("#temasEditor").html(
        '<option>default </option>' +
        '<option>ambiance</option>' +
        '<option>cobalt</option>' +
        '<option>dracula</option>' +
        '<option>eclipse</option>' +
        '<option>material</option>' +
        '<option>mbo</option>' +
        '<option>mdn-like</option>' +
        '<option>neat</option>' +
        '<option>rubyblue</option>' +
        '<option>solarized dark</option>' +
        '<option>twilight</option>' +
        '<option>zenburn</option>'
    );

    $scope.renderHtml = function(code) {
        return $sce.trustAsHtml(code);
    };

    // $('#modalInfoDesafio').modal('show');

    var getDesafio = function() {
        var def = jQuery.Deferred();
        $http.get(host + 'desafio/' + $rootScope.idDesafio).then(
            function success(res) {
                def.resolve(res.data[0]);
            },
            function error(err) {
                def.reject(err);
                notification('erro', 'FALHA', 'Falha ao obter o desafio!');
            }
        );
        return def.promise();
    }

    function loadDesafio() {
        if ($localStorage.idDesafioAtual > 0) {
            getDesafio().then(function(data) {
                $scope.desafio = data;
                $scope.desafio.Config = tipoDesafio(data.Dificuldade);
                $scope.desafio.Comprador = $localStorage.usuarioLogado;
                if ($localStorage.codigoEditor != $scope.desafio.PreCodigo && $localStorage.codigoEditor != '') {
                    $scope.resposta = $localStorage.codigoEditor;
                } else {
                    $scope.resposta = $scope.desafio.PreCodigo;
                    $localStorage.codigoEditor = $scope.desafio.PreCodigo;
                }
                if ($scope.desafio.Tempo != "00:00:00") {
                    var tempo = $scope.desafio.Tempo.split(':');
                    $scope.tempoDesafio = (tempo[0]) * 60 * 60 + (tempo[1]) * 60;
                } else {
                    $scope.tempoDesafio = 0;
                }
            });
            $scope.tempoDesafio = $localStorage.tempo;
        } else {
            location.assign('index.html#/desafios');
            notification('aviso', 'ATENÇÃO', 'Para participar desse desafio você deve compra-lo!');
        }
    }
    loadDesafio();

    $('#editor').keyup(function() {
        $localStorage.codigoEditor = $scope.resposta;
    });

    $scope.$on('timer-tick', function(event, data) {
        if ($scope.tempoDesafio > 0) {
            $localStorage.tempo = parseInt(data.millis / 1000);
        }
    });

    $scope.timerRunning = true;
    $scope.startTimer = function() {
        $scope.$broadcast('timer-start');
        $scope.timerRunning = true;
    };

    $scope.stopTimer = function() {
        $scope.$broadcast('timer-stop');
        $scope.timerRunning = false;
    };

    $scope.respondendo = true;
    $scope.$on('timer-stopped', function(event, data) {
        notification('aviso', 'AVISO', 'O tempo para responder o desafio acabou!');
        $scope.editorOptions.readOnly = 'nocursor';
        $scope.respondendo = false;
    });

    $scope.responder = function(desafio) {
        if ($scope.resposta != "") {
            swal({
                title: "Realmente deseja confirmar sua resposta?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Sim",
                cancelButtonText: "Cancelar",
                closeOnConfirm: true,
                timer: 2500
            }, function(isConfirm) {
                if (isConfirm) {
                    var codigo = String($scope.resposta).split("\n");
                    var entrada = "";
                    var qtd = 0;
                    //Gera o novo código substituindo a função: input(), pelas entradas pré cadastradas
                    for (i = 0; i < codigo.length; i++) {
                        if (!codigo[i].includes('input()')) {
                            entrada += String(codigo[i]) + "\n";
                        } else {
                            entrada += String(codigo[i].replace('input()', desafio.Entradas[0].Valor.split('\n')[qtd])) + "\n";
                            qtd++;
                        }
                    }
                    var saidasDesafio = desafio.Saidas[0].Valor.split('\n');
                    desafio.Resposta = $scope.resposta;
                    executaCodigoModificado(entrada);
                    var saidas = getSaidaExecucao();
                    var listaResposta = saidas;

                    if (listaResposta.length == saidasDesafio.length) {
                        var correto = 0;
                        for (i = 0; i < saidasDesafio.length; i++) {
                            if (!isNaN(listaResposta[i]) && !isNaN(saidasDesafio[i])) {
                                listaResposta[i] = Number(listaResposta[i]);
                                saidasDesafio[i] = Number(saidasDesafio[i]);
                            }
                            if (saidasDesafio[i] == listaResposta[i]) {
                                correto++;
                            }
                        }
                        if (correto == listaResposta.length) {
                            notification('sucesso', 'SUCESSO', 'Resposta correta!');
                            desafio.DesafioCorreto = true;
                        } else {
                            notification('erro', 'ERRO', 'Resposta incorreta!');
                            desafio.DesafioCorreto = false;
                        }
                        $http.post(host + 'responder-desafio', desafio).then(
                            function success(res) {
                                $rootScope.usuarioLogado = res.data;
                                $localStorage.usuarioLogado = res.data;
                                updateConquistaUsuario();
                            },
                            function error() {
                                notification('erro', 'FALHA', 'Falha ao responder desafio!');
                            }
                        );
                        socket.emit('atualizarRanking');
                        $location.path('/desafios');
                    } else {
                        notification('erro', 'ERRO', 'Resposta incorreta!');
                        desafio.DesafioCorreto = false;
                        $http.post(host + 'responder-desafio', desafio).then(
                            function success(response) {
                                $localStorage.usuarioLogado = response.data;
                                updateConquistaUsuario();
                            },
                            function error() {
                                notification('erro', 'FALHA', 'Falha ao responder desafio!');
                            }
                        );
                        socket.emit('atualizarRanking');
                        $location.path('/desafios');
                    }
                }
            });
        } else {
            notification("aviso", "AVISO", "Digite seu código!");
        }
    }

    if ($localStorage.tema == null) {
        $localStorage.tema = { nome: 'default', index: 0 };
    } else {
        $('#temasEditor option:eq(' + $localStorage.tema.index + ')').attr('selected', 'selected');
    }

    $scope.editorOptions = {
        lineNumbers: true,
        styleActiveLine: true,
        matchBrackets: true,
        indentUnit: 4,
        autoMatchParens: true,
        parserConfig: { 'pythonVersion': 2, 'strictErrors': true },
        theme: $localStorage.tema.nome,
        readOnly: false
    };

    $("#temasEditor").change(function() {
        $localStorage.tema = { nome: $("#temasEditor").val(), index: $("#temasEditor")[0].selectedIndex };
        location.reload();
    });

    //***************** Execução do código
    var mypre = document.getElementById("output");
    var outf = function(text) {
        mypre.innerHTML = mypre.innerHTML + text;
    }

    var builtinRead = function(x) {
        if (Sk.builtinFiles === undefined || Sk.builtinFiles["files"][x] === undefined)
            throw "File not found: '" + x + "'";
        return Sk.builtinFiles["files"][x];
    }

    $scope.executarCodigo = function() {
        if ($scope.resposta != "") {
            var prog = $scope.resposta;
            mypre.innerHTML = '';
            Sk.pre = "output";
            Sk.configure({ output: outf, read: builtinRead });
            (Sk.TurtleGraphics || (Sk.TurtleGraphics = {})).target = 'mycanvas';
            var myPromise = Sk.misceval.asyncToPromise(function() {
                return Sk.importMainWithBody("<stdin>", false, prog, true);
            });
            myPromise.then(function(mod) {
                console.log("sucesso");
            }, function(err) {
                $("#output").html('<span style="color: red">' + err.toString() + '</span>');
            });
        } else {
            notification("aviso", "AVISO", "Digite seu código!");
        }
    }

    var executaCodigoModificado = function(entrada) {
        mypre.innerHTML = '';
        Sk.pre = "output";
        Sk.configure({ output: outf, read: builtinRead });
        (Sk.TurtleGraphics || (Sk.TurtleGraphics = {})).target = 'mycanvas';
        var myPromise = Sk.misceval.asyncToPromise(function() {
            return Sk.importMainWithBody("", false, entrada, true);
        });
        myPromise.then(function(mod) {
            console.log("sucesso!");
        }, function(err) {
            console.log(err.toString());
        });
    }

    var getSaidaExecucao = function() {
        var saidas = $(mypre)[0].outerHTML.replace('<pre id="output" ng-show="saida" class="ng-hide">', '').replace('</pre>', '').split('\n');
        saidas = saidas.splice(0, saidas.length - 1);
        if (String(saidas).includes('<pre')) {
            saidas = String(saidas).replace('<pre id="output" ng-show="saida" class="">', '').split(',');
        }
        return saidas;
    }

    var updateConquistaUsuario = function() {
        var usuario = getEvolucaoConquistas($localStorage.usuarioLogado);
        if (usuario != 0) {
            var add = false;
            $localStorage.conquistasUsuario.forEach(function(conquista) {
                if (!conquista.Possui && usuario.Conquista == conquista.Nome) {
                    add = true;
                }
            });
            if (add && usuario != undefined) {
                $http.post(host + 'set-conquista-usuario', JSON.stringify({ usuario: usuario })).then(
                    function success(res) {
                        notification('info', 'AVISO', 'Você ganhou o prêmio: ' + usuario.Conquista);
                        ion.sound.play('what-another-message');
                    },
                    function error(err) {
                        notification('erro', 'FALHA', 'Falha ao atualizar a conquista do usuário!');
                    }
                );
            }
        }
    }

    function getEvolucaoConquistas(usuario) {
        if (usuario.Classe == 'Novato' && usuario.Nivel == 2) {
            return {
                Conquista: 'Primeira evolução',
                Id: usuario.Id
            };
        }
        if (usuario.Classe == 'Padawan' && usuario.Nivel == 1) {
            return {
                Conquista: 'Aprendiz Jedi',
                Id: usuario.Id
            };
        }
        if (usuario.Classe == 'Padawan' && usuario.Nivel == 5) {
            return {
                Conquista: 'Obi-Wan Kenobi está orgulhoso',
                Id: usuario.Id
            };
        }
        if (usuario.Classe == 'Cavaleiro Jedi' && usuario.Nivel == 1) {
            return {
                Conquista: 'Novo Jedi',
                Id: usuario.Id
            };
        }
        if (usuario.Classe == 'Cavaleiro Jedi' && usuario.Nivel == 5) {
            return {
                Conquista: 'Quase um Anakin Skywalker',
                Id: usuario.Id
            };
        }
        if (usuario.Classe == 'Cavaleiro Jedi' && usuario.Nivel == 7) {
            return {
                Conquista: 'A Força está com você!',
                Id: usuario.Id
            };
        }
        if (usuario.Classe == 'Mestre Jedi' && usuario.Nivel == 1) {
            return {
                Conquista: 'Um verdadeiro cavaleiro, você é',
                Id: usuario.Id
            };
        }
        if (usuario.Classe == 'Mestre Jedi' && usuario.Nivel == 3) {
            return {
                Conquista: 'O mestre dos mestres',
                Id: usuario.Id
            };
        }
        if (usuario.Classe == 'Mestre do conselho' && usuario.Nivel == 1) {
            return {
                Conquista: 'Superando o Mestre Yoda',
                Id: usuario.Id
            };
        }
        return 0;
    }
});