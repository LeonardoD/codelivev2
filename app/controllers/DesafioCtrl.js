app.controller("DesafioCtrl", function($localStorage, $rootScope, $scope, $modal, $http, validaDesafio, $location) {
    $localStorage.tempo = 0;
    $localStorage.idDesafioAtual = 0
    $localStorage.codigoEditor = '';
    $scope.open = function(size, item) {
        $rootScope.configDesafio = tipoDesafio(item.Dificuldade);
        $rootScope.desafioDetalhes = item;
        $rootScope.TagsDesafio = [];
        $rootScope.TagsDesafio = organizaTags(item);
        var modalInstance = $modal.open({
            templateUrl: "views/modal/modalDesafio.html",
            controller: "ModalController",
            size: size,
            resolve: {
                model: function() {
                    return [];
                }
            }
        });
    };

    $scope.abrirLink = function(link) {
        window.location.href = link;
    }

    var getDesafios = function() {
        $http.get(host + "desafio").then(
            function success(res) {
                $scope.desafios = res.data;
                if (res.data.length > 0) {
                    for (var i = 0; i < res.data.length; i++) {
                        $scope.desafios[i].Config = tipoDesafio(res.data[i].Dificuldade);
                    }
                }
            }
        );
    }
    getDesafios();

    $rootScope.comprarDesafio = function(desafio) {
        $http.post(host + 'verifica-desafio-vencido',
            JSON.stringify({
                dados: {
                    idDesafio: desafio.Id,
                    idUsuario: $rootScope.usuarioLogado.Id
                }
            })).then(
            function success(res) {
                if (res.data) {
                    notification('aviso', 'ATENÇÃO', 'Você não pode comprar esse desafio pois já o venceu!');
                } else {
                    if (validaDesafio.validaCompraDesafio(desafio.Dificuldade, $rootScope.usuarioLogado)) {
                        if ($localStorage.idUsuarioLogado != desafio.Autor) {
                            if ($rootScope.usuarioLogado.Credito >= desafio.Config.requisitos.creditosCobrados) {
                                desafio.Comprador = $rootScope.usuarioLogado;
                                $http.post(host + "comprar-desafio", JSON.stringify({ desafio: desafio })).then(
                                    function success(response) {
                                        $rootScope.closeModalDesafios();
                                        $rootScope.responderDesafio = desafio;
                                        var tempo = desafio.Tempo.split(':');
                                        $localStorage.tempo = (tempo[0]) * 60 * 60 + (tempo[1]) * 60;
                                        $localStorage.idDesafioAtual = desafio.Id;
                                        $localStorage.usuarioLogado = response.data;
                                        $location.path("/responder-desafio/" + desafio.Id);
                                        notification("sucesso", "SUCESSO", "Desafio comprado com sucesso!");
                                    },
                                    function error() {
                                        notification("erro", "FALHA", "Falha ao comprar desafio!");
                                    }
                                );
                            } else {
                                notification("aviso", "AVISO", "Você não possui crédito suficiente para comprar esse desafio!");
                            }
                        } else {
                            notification("aviso", "AVISO", "Você só pode comprar desafios criados por outros usuários!");
                        }
                    } else {
                        notification("aviso", "AVISO", "Você não possui os requisitos necessários para comprar este desafio!");
                    }
                }
            },
            function error(err) {
                notification("erro", "FALHA", "Falha ao comprar desafio!");
            });
    }

    socket.on('novoDesafio', function() {
        getDesafios();
    });

    socket.on('updateStatusDesafio', function() {
        getDesafios();
    });
});