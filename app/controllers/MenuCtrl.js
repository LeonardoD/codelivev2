app.controller("MenuCtrl", function ($localStorage, $rootScope, $scope, $http, usuarioLogado, $location) {

    usuarioLogado.verificaUsuarioLogado();
    $scope.logout = function () {
        $http.get(host + "logoff").then(
            function success(res) {
                $localStorage.usuarioLogado = null;
                $localStorage.idUsuarioLogado = null;
                localStorage.setItem('logged', false);
                socket.emit('logout');
                $location.path("/login");
            }
        );
    }
});