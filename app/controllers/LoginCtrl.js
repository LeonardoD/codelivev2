app.controller("LoginCtrl", function($localStorage, $rootScope, $scope, $http, usuarioLogado, $location) {
    var verificaUsuarioLogado = function() {
        $http.get(host + 'get-usuario-logado').then(
            function success(res) {
                if (res.data != "") {
                    $location.path("/perfil");
                } else {
                    localStorage.setItem('logged', false);
                    $location.path("/login");
                }
            },
            function errro(err) {
                $location.path("/login");
            }
        );
    }
    verificaUsuarioLogado();

    $scope.login = function(usuario) {
        if ($scope.usuario.Senha != undefined && $scope.usuario.NomeExibicao != undefined) {
            $http.post(host + "login", JSON.stringify({ usuario: usuario })).then(
                function success(res) {
                    if (res.data != "") {
                        socket.emit("novoUsuarioLogado", res.data);
                        $localStorage.usuarioLogado = res.data;
                        $localStorage.idUsuarioLogado = res.data.Id;
                        $location.path("/perfil");
                    } else {
                        notification("aviso", "FALHA", "<strong>Usuário</strong> ou <strong>Senha</strong> Incorretos");
                    }
                },
                function error(err) {
                    notification("erro", "FALHA", "Falha ao fazer o login!");
                }
            );
        } else {
            notification("aviso", "ATENÇÃO", "Preencha todos os campos!");
        }
    }
    $scope.wainSolicitation = false;
    $scope.emailUsuario = 'yoda@gmail.com';
    $scope.recuperarSenha = function() {
        var usuarioSolicitante;
        $http.get(host + 'usuario').then(
            function success(response) {
                response.data.forEach(function(usuario) {
                    if ($scope.emailUsuario == usuario.Email) {
                        usuarioSolicitante = usuario;
                    }
                });
                if (usuarioSolicitante != undefined) {
                    $scope.wainSolicitation = true;
                    var usuario = {
                        Id: usuarioSolicitante.Id,
                        NovaSenha: 'codelive' + new Date().getFullYear()
                    };
                    $http.post(host + 'send-email', JSON.stringify({ usuario: usuario })).then(
                        function success(res) {
                            $http.put(host + "update-senha-usuario/" + usuario.Id, JSON.stringify({ usuario: usuario })).then(
                                function success() {
                                    notification('sucesso', 'SUCESSO', 'Solicitação enviada com sucess!');
                                    $('#modalRecuperarSenha').modal('hide');
                                    $scope.wainSolicitation = false;
                                    $scope.emailUsuario = null;
                                },
                                function error(err) {
                                    console.log(err);
                                    notification('erro', 'FALHA', 'Falha ao enviar a solicitação!');
                                }
                            );
                        },
                        function error(err) {
                            console.log(err);
                            notification('erro', 'FALHA', 'Falha ao enviar a solicitação!');
                        }
                    );

                } else {
                    notification('aviso', 'AVISO', 'E-mail não cadastrado!');
                }
            },
            function error(err) {
                console.log(err);
            }
        );
    }
});