app.controller("GerenciarMeusDesafiosCtrl", function ($localStorage, $rootScope, $scope, $http, $modal, $q, $sce, $location) {

    $scope.renderHtml = function (code) {
        return $sce.trustAsHtml(code);
    };

    var getDesafios = function () {
        $http.get(host + "desafios-por-usuario/" + $localStorage.idUsuarioLogado).then(
            function success(res) {
                $scope.desafios = res.data;
                for (var i = 0; i < res.data.length; i++) {
                    $scope.desafios[i].Config = tipoDesafio(res.data[i].Dificuldade);
                }
            });
    }
    getDesafios();

    $scope.publicarDesafio = function (desafio) {
        $http.put(host + "ativar-desafio/" + desafio.Id).then(
            function success() {
                getDesafios();
                notification("sucesso", "SUCESSO", "Desafio publicado!");
            }, function error() {
                notification("erro", "FALHA", "Falha ao publicar desafio!");
            }
        );
    }

    $scope.desativarDesafio = function (desafio) {
        $http.put(host + "desativar-desafio/" + desafio.Id).then(
            function success() {
                getDesafios();
                notification("sucesso", "SUCESSO", "Desafio desativo!");
            }, function error() {
                notification("erro", "FALHA", "Falha ao desativar desafio!");
            }
        );
    }

    $rootScope.detalhesDesafio = function (size, item) {
        getDesafios();
        $rootScope.configDesafio = tipoDesafio(item.Dificuldade);
        $rootScope.desafioDetalhes = item;
        $rootScope.desafioDetalhes.Entradas = item.Entradas[0];
        $rootScope.desafioDetalhes.Saidas = item.Saidas[0];
        $rootScope.TagsDesafio = organizaTags(item.Tags);
        desempenhoGeral(item.Id);
        var modalInstance = $modal.open({
            templateUrl: "views/modal/modalDetalhesMeuDesafio.html",
            controller: "ModalController",
            size: size,
            resolve: {
                model: function () {
                    return [];
                }
            }
        });
    };

    $scope.editarDesafio = function (id) {
        $http.get(host + "verifica-historico-desafio/" + id).success(function (data) {
            if (data) {
                $location.path("/gerenciar-meus-desafios");
                notification('aviso', 'ATENÇÃO', 'O desafio não pode ser editado pois já foi comprado por alguém!');
            } else {
                $location.path('/editar-desafio/' + id);
            }
        });
    }

    function organizaTags(tgs) {
        var tagsDesafio = [];
        var aux = [];
        var qt = tgs.indexOf(",") + 1;
        if (qt > 0) {
            for (var i = 0; i < qt; i++) {
                aux.push(tgs.split(",")[i]);
                if (aux[i] != undefined) {
                    tagsDesafio.push(tgs.split(",")[i]);
                }
            }
        } else {
            tagsDesafio.push(tgs);
        }
        return tagsDesafio;
    }

    var getHistorico = function (idDesafio) {
        var def = jQuery.Deferred();
        $http.get(host + 'historico-desafio/' + idDesafio).then(
            function success(res) {
                def.resolve(res.data);
            }, function error(err) {
                def.reject(err);
            }
        );
        return def.promise();
    }

    var desempenhoGeral = function (idDesafio) {
        getHistorico(idDesafio).then(function (historico) {
            var v = 0;
            var d = 0;
            historico.forEach(function (data) {
                if (data.DesafioVencido) {
                    v++;
                } else {
                    d++;
                }
            });
            $rootScope.vitorias = v;
            $rootScope.derrotas = d;
            $rootScope.dgVitoriasP = v > 0 ? ((v * 100) / (v + d)).toFixed(2) : 0;
            $rootScope.dgDerrotasP = d > 0 ? ((d * 100) / (v + d)).toFixed(2) : 0;
        });
    }
});
