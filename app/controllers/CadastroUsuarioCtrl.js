app.controller("CadastroUsuarioCtrl", function($scope, $http, $q, $location) {
    $scope.usuario;
    $scope.cadastrar = function(usuario) {
        usuario.IdImagem = $('input[name=imagemAvatar]:checked').val();
        if (usuario.Nome != undefined && usuario.Email != undefined &&
            usuario.NomeExibicao != undefined && usuario.Senha != undefined &&
            usuario.SenhaConfirmacao != undefined) {
            if (usuario.Senha == usuario.SenhaConfirmacao) {
                verificaExitenciaUsuario(usuario.NomeExibicao).then(function(res) {
                    if (!res) {
                        if (usuario.IdImagem == undefined) {
                            usuario.IdImagem = 1;
                        }
                        $http.post(host + "usuario", JSON.stringify({ usuario: usuario })).then(
                            function success(res) {
                                if (res.data > 0) {
                                    delete $scope.usuario;
                                    notification("sucesso", "SUCESSO", "Usuário cadastrado com sucesso!");
                                    $location.path("/login");
                                } else {
                                    notification("erro", "FALHA", "Falha ao cadastrar usuário!");
                                }
                            },
                            function error() {
                                notification("erro", "FALHA", "Falha ao cadastrar usuário!");
                            }
                        );
                    } else {
                        notification('aviso', 'ATENÇÃO', 'Já existe um usuário com esse nome de exibição');
                    }
                });
            } else {
                notification("aviso", "ATENÇÃO", "Senhas são diferentes!");
            }
        } else {
            notification("aviso", "ATENÇÃO", "Preencha todos os campos!");
        }
    }

    var getAvatares = function() {
        $http.get(host + 'get-avatares').then(
            function success(res) {
                $scope.avatares = res.data;
            },
            function error() {
                notification("erro", "FALHA", "Falha ao exibir os avatares!");
            }
        );
    }

    var verificaExitenciaUsuario = function(nomeExibicao) {
        var deffered = $q.defer();
        var existe = false;
        $http.get(host + 'usuario').then(
            function success(res) {
                $.each(res.data, function(i, usuario) {
                    if (usuario.NomeExibicao == nomeExibicao) {
                        existe = true;
                    }
                });
                deffered.resolve(existe);
            })
        return deffered.promise;
    }
    getAvatares();
});