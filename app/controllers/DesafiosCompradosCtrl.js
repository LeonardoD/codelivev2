app.controller('DesafiosCompradosCtrl', function ($scope, $rootScope, $http, $localStorage, $sce) {
    $scope.desafios = [];
    $localStorage.codigoEditorTreino = undefined;
    $scope.renderHtml = function (code) {
        return $sce.trustAsHtml(code);
    };


    var getDesafiosComprados = function () {
        $http.get(host + "get-desafios-comprados/" + $localStorage.idUsuarioLogado).then(
            function succes(res) {
                $scope.desafios = res.data;
                $scope.desafios.forEach(function (desafio) {
                    if (desafio.IdDesafio == $rootScope.idDesafio) {
                        desempenhoGeral(desafio.Historico);
                        $scope.desafio = desafio;
                    }
                })
            }, function error() {
                notification('erro', 'FALHA', 'Falha ao listar os desafio comprados!');
            }
        );
    }
    getDesafiosComprados();


    var desempenhoGeral = function (historico) {
        var v = 0;
        var d = 0;
        historico.forEach(function (data) {
            if (data.DesafioVencido) {
                v++;
            } else {
                d++;
            }
        });
        $rootScope.vitorias = v;
        $rootScope.derrotas = d;
        $rootScope.dgVitoriasP = v > 0 ? ((v * 100) / (v + d)).toFixed(2) : 0;
        $rootScope.dgDerrotasP = d > 0 ? ((d * 100) / (v + d)).toFixed(2) : 0;
        $rootScope.totalParticipacao = v + d;
    }
});