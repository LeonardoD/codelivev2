app.controller("UsuarioCtrl", function($rootScope, $scope, $http, fileUpload, $localStorage, $q, usuarioLogado) {
    usuarioLogado.verificaUsuarioLogado();
    $scope.conquistasUsuario = [];

    $scope.enviar = function() {
        socket.emit('msgSk', $scope.idSocket);
    }

    var verificaExitenciaUsuario = function(nomeExibicao, idUsuario) {
        var deffered = $q.defer();
        var existe = false;

        $http.get(host + 'usuario').then(function success(res) {
            $.each(res.data, function(i, usuario) {
                if (usuario.NomeExibicao == nomeExibicao && usuario.Id != idUsuario) {
                    existe = true;
                }
            });
            deffered.resolve(existe);
        })
        return deffered.promise;
    }

    $scope.updateUsuario = function(usuario) {
        if (usuario.Nome != undefined || usuario.Email != undefined || usuario.NomeExibicao != undefined) {
            verificaExitenciaUsuario(usuario.NomeExibicao, usuario.Id).then(function(res) {
                if (res) {
                    notification('aviso', 'ATENÇÃO', 'Já existe um usuário com esse nome de exibição');
                } else {
                    $http.put(host + "usuario/" + usuario.Id, JSON.stringify({ usuario: usuario })).then(
                        function success(response) {
                            $rootScope.usuarioLogado = response.data;
                            $localStorage.usuarioLogado = response.data;
                            $('#modalDados').modal('hide');
                            notification("sucesso", "SUCESSO", "Perfil atualizado com sucesso!");
                        },
                        function error() {
                            notification("erro", "FALHA", "Falha ao atualizar!");
                        }
                    );
                }
            });
        } else {
            notification("aviso", "ATENÇÃO", "Preencha todos os campos!")
        }
    }

    $scope.updateSenhaUsuario = function(usuario) {
        if (usuario.NovaSenha != undefined && usuario.SenhaConfirmacao != undefined) {
            if (usuario.NovaSenha == usuario.SenhaConfirmacao) {
                usuario.Senha = usuario.NovaSenha;
                $http.put(host + "update-senha-usuario/" + usuario.Id, JSON.stringify({ usuario: usuario })).then(
                    function success(res) {
                        if (res.data == 1) {
                            $('#modalSenha').modal('hide');
                            notification("sucesso", "SUCESSO", "Senha atualizada com sucesso!");
                            $scope.usuarioLogado.NovaSenha = null;
                            $scope.usuarioLogado.SenhaConfirmacao = null;
                        } else {
                            notification("erro", "FALHA", "Falha ao atualizar!")
                        }
                    }
                );
            } else {
                notification("aviso", "AVISO", "Senhas são diferentes!")
            }
        } else {
            notification("aviso", "AVISO", "Preencha todos os campos!")
        }
    }

    $scope.uploadFile = function() {
        var file = $scope.arquivo;
        var uploadUrl = host + "upload-imagem";
        fileUpload.uploadFileToUrl(file, uploadUrl).then(function(res) {
            if (res) {
                $('#modalFoto').modal('hide');
                usuarioLogado.getInformacoes();
            }
        });
    };

    var getUsuariosRanking = function() {
        $http.get(host + 'usuario').then(
            function success(res) {
                $scope.rankingUsuarios = res.data;
            },
            function error() {
                notification('erro', 'FALHA', 'Falha ao exibir os usuário do Ranking!');
            }
        );
    }
    getUsuariosRanking();

    socket.on('atualizarRanking', function() {
        $scope.update = true;
        getUsuariosRanking();
    });

    var getConquistas = function() {
        var d = jQuery.Deferred();
        $http.get('../conquistas.json').then(
            function success(res) {
                d.resolve(res.data);
            }
        );
        return d.promise();
    }

    var getConquistasUsuario = function() {
        $http.get(host + 'get-conquistas-usuario/' + $localStorage.idUsuarioLogado)
            .then(function success(conquistasUsuario) {
                getConquistas().then(function(conquistas) {
                    conquistas.forEach(function(conquista) {
                        conquistasUsuario.data.forEach(function(conquistaUsuario) {
                            if (conquistaUsuario.Nome == conquista.Nome) {
                                conquista.Possui = true;
                            }
                        });
                        $scope.conquistasUsuario.push(conquista);
                        $localStorage.conquistasUsuario = $scope.conquistasUsuario;
                    });
                });
            }, function error() {
                notification('erro', '', 'Falha para obter as conquistas do usuário!');
            });
    }
    getConquistasUsuario();

    var getHistorico = function() {
        var def = jQuery.Deferred();
        $http.get(host + 'historico-desafio-usuario/' + $localStorage.idUsuarioLogado)
            .then(function success(res) {
                def.resolve(res.data);
            }, function error(err) {
                def.reject(err);
            });
        return def.promise();
    }

    var getDesafios = function() {
        var def = jQuery.Deferred();
        $http.get(host + 'desafio').then(
            function success(res) {
                def.resolve(res.data);
            },
            function error(err) {
                def.reject(err);
            }
        );
        return def.promise();
    }

    //Métodos de exebição das estatísticas do usuário logado no jogo
    var desempenhoGeral = function() {
        getHistorico().then(function(historico) {
            var v = 0;
            var d = 0;
            historico.forEach(function(data) {
                if (data.DesafioVencido) {
                    v++;
                } else {
                    d++;
                }
            });
            $scope.dgVitorias = v;
            $scope.dgDerrotas = d;
            $scope.dgVitoriasP = Number(v > 0 ? ((v * 100) / (v + d)).toFixed(2) : 0);
            $scope.dgDerrotasP = Number(d > 0 ? ((d * 100) / (v + d)).toFixed(2) : 0);
        });
    }
    desempenhoGeral();

    var desempenhoNivelDificuldade = function() {
        var qtdDd1 = 0;
        var qtdDd2 = 0;
        var qtdDd3 = 0;
        var qtdDd4 = 0;
        var n1 = 0;
        var n2 = 0;
        var n3 = 0;
        var n4 = 0;
        getHistorico().then(function(historico) {
            historico.forEach(function(data) {
                if (data.Dificuldade == 1 && data.DesafioVencido) {
                    n1++;
                }
                if (data.Dificuldade == 2 && data.DesafioVencido) {
                    n2++;
                }
                if (data.Dificuldade == 3 && data.DesafioVencido) {
                    n3++;
                }
                if (data.Dificuldade == 4 && data.DesafioVencido) {
                    n4++;
                }
            });

            getDesafios().then(function(desafios) {
                desafios.forEach(function(desafio) {
                    if (desafio.Dificuldade == 1) {
                        qtdDd1++;
                    }
                    if (desafio.Dificuldade == 2) {
                        qtdDd2++;
                    }
                    if (desafio.Dificuldade == 3) {
                        qtdDd3++;
                    }
                    if (desafio.Dificuldade == 4) {
                        qtdDd4++;
                    }
                });
                //número real de vitórios
                $scope.dnf1 = n1;
                $scope.dnf2 = n2;
                $scope.dnf3 = n3;
                $scope.dnf4 = n4;
                //porcentagem de vitórias pelos desafios existentes
                $scope.dnf1P = Number(qtdDd1 > 0 ? ((n1 * 100) / qtdDd1).toFixed(2) : 0);
                $scope.dnf2P = Number(qtdDd2 > 0 ? ((n2 * 100) / qtdDd2).toFixed(2) : 0);
                $scope.dnf3P = Number(qtdDd3 > 0 ? ((n3 * 100) / qtdDd3).toFixed(2) : 0);
                $scope.dnf4P = Number(qtdDd4 > 0 ? ((n4 * 100) / qtdDd4).toFixed(2) : 0);
            });
        });
    }
    desempenhoNivelDificuldade();

    var desempenhoClasse = function() {
        if ($localStorage.usuarioLogado != null) {
            var usuario = $localStorage.usuarioLogado;
            var proximaClasse = {};
            if (usuario.Classe == 'Mestre do conselho') {
                $scope.progressoEvo = 'mc';
            } else {
                $http.get('../classes.json').then(
                    function success(classes) {
                        classes.data.forEach(function(classe) {
                            if (usuario.Classe == classe.classe && usuario.Nivel + 1 == classe.nivel) {
                                proximaClasse = classe;
                            }
                        });
                        $scope.dfParaEvoluir = {
                            dfEvo: proximaClasse.dfInicio - usuario.NivelDf,
                            classeEvo: proximaClasse.classe,
                            nivelEvo: proximaClasse.nivel
                        };
                        $scope.progressoEvo = Number(((usuario.NivelDf * 100) / proximaClasse.dfInicio).toFixed(2));
                    }
                );
            }
        }
    }
    desempenhoClasse();
});