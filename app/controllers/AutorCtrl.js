app.controller('AutorCtrl', function($scope, $rootScope, $http, $localStorage) {

    $scope.conquistasAutor = [];

    var getDesafios = function() {
        var def = jQuery.Deferred();
        $http.get(host + 'desafio').then(
            function succcess(res) {
                def.resolve(res.data);
            },
            function error(err) {
                def.reject(err);
            }
        );
        return def.promise();
    }

    var getConquistas = function() {
        var d = jQuery.Deferred();
        $http.get('../conquistas.json').then(
            function success(res) {
                d.resolve(res.data);
            }
        );
        return d.promise();
    }

    var getHistoricoAutor = function(idUsuario) {
        var def = jQuery.Deferred();
        $http.get(host + 'historico-desafio-usuario/' + idUsuario).then(
            function success(res) {
                def.resolve(res.data);
            },
            function error(err) {
                def.reject(err);
            }
        );
        return def.promise();
    }

    var getConquistasAutor = function(idUsuario) {
        $http.get(host + 'get-conquistas-usuario/' + idUsuario).then(
            function success(conquistasUsuario) {
                getConquistas().then(function(conquistas) {
                    conquistas.forEach(function(conquista) {
                        conquistasUsuario.data.forEach(function(conquistaUsuario) {
                            if (conquistaUsuario.Nome == conquista.Nome) {
                                conquista.Possui = true;
                            }
                        });
                        $scope.conquistasAutor.push(conquista);
                        $localStorage.conquistasAutor = $scope.conquistasAutor;
                    });
                });
            },
            function error() {
                notification('erro', '', 'Falha para obter as conquistas do usuário!');
            }
        );
    }

    var desempenhoGeralAutor = function() {
        getHistoricoAutor().then(function(historico) {
            var v = 0;
            var d = 0;
            historico.forEach(function(data) {
                if (data.DesafioVencido) {
                    v++;
                } else {
                    d++;
                }
            });
            $scope.dgVitorias = v;
            $scope.dgDerrotas = d;
            $scope.dgVitoriasP = Number(v > 0 ? ((v * 100) / (v + d)).toFixed(2) : 0);
            $scope.dgDerrotasP = Number(d > 0 ? ((d * 100) / (v + d)).toFixed(2) : 0);
        });
    }

    var desempenhoNivelDificuldadeAutor = function(idUsuario) {
        var qtdDd1 = 0;
        var qtdDd2 = 0;
        var qtdDd3 = 0;
        var qtdDd4 = 0;
        var n1 = 0;
        var n2 = 0;
        var n3 = 0;
        var n4 = 0;
        getHistoricoAutor(idUsuario).then(function(historico) {
            historico.forEach(function(data) {
                if (data.Dificuldade == 1 && data.DesafioVencido) {
                    n1++;
                }
                if (data.Dificuldade == 2 && data.DesafioVencido) {
                    n2++;
                }
                if (data.Dificuldade == 3 && data.DesafioVencido) {
                    n3++;
                }
                if (data.Dificuldade == 4 && data.DesafioVencido) {
                    n4++;
                }
            });

            getDesafios().then(function(desafios) {
                desafios.forEach(function(desafio) {
                    if (desafio.Dificuldade == 1) {
                        qtdDd1++;
                    }
                    if (desafio.Dificuldade == 2) {
                        qtdDd2++;
                    }
                    if (desafio.Dificuldade == 3) {
                        qtdDd3++;
                    }
                    if (desafio.Dificuldade == 4) {
                        qtdDd4++;
                    }
                });
                //número real de vitórios
                $scope.dnf1 = n1;
                $scope.dnf2 = n2;
                $scope.dnf3 = n3;
                $scope.dnf4 = n4;
                //porcentagem de vitórias pelos desafios existentes
                $scope.dnf1P = Number(qtdDd1 > 0 ? ((n1 * 100) / qtdDd1).toFixed(2) : 0);
                $scope.dnf2P = Number(qtdDd2 > 0 ? ((n2 * 100) / qtdDd2).toFixed(2) : 0);
                $scope.dnf3P = Number(qtdDd3 > 0 ? ((n3 * 100) / qtdDd3).toFixed(2) : 0);
                $scope.dnf4P = Number(qtdDd4 > 0 ? ((n4 * 100) / qtdDd4).toFixed(2) : 0);
            });
        });
    }

    var desempenhoClasseAutor = function(usuario) {
        var proximaClasse = {};
        if (usuario.Classe == 'Mestre do conselho') {
            $scope.progressoEvo = 'mc';
        } else {
            $http.get('../classes.json').then(
                function success(classes) {
                    classes.data.forEach(function(classe) {
                        if (usuario.Classe == classe.classe && usuario.Nivel + 1 == classe.nivel) {
                            proximaClasse = classe;
                        }
                    });
                    $scope.dfParaEvoluir = {
                        dfEvo: proximaClasse.dfInicio - usuario.NivelDf,
                        classeEvo: proximaClasse.classe,
                        nivelEvo: proximaClasse.nivel
                    };
                    $scope.progressoEvo = Number(((usuario.NivelDf * 100) / proximaClasse.dfInicio).toFixed(2));
                }
            );
        }
    }

    var loadPerfilAutor = function() {
        $http.get(host + 'usuario/' + $rootScope.idUsuario).then(
            function success(res) {
                $scope.autor = res.data;
                getConquistasAutor(res.data.Id);
                desempenhoGeralAutor();
                desempenhoClasseAutor(res.data);
                desempenhoNivelDificuldadeAutor(res.data.Id);
            },
            function error() {
                notification('erro', '', 'Falha para obter os dados do Autor!');
            }
        );
    }
    loadPerfilAutor();
});