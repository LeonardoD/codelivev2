app.controller("ModalController", function ($rootScope, $scope, $modalInstance, model, $sce) {

    $scope.abrirLink = function (link) {
        location.href = link + ".html";
    }

    $scope.renderHtml = function (code) {
        return $sce.trustAsHtml(code);
    };

    $scope.model = model;
    $scope.aux = [];
    $scope.rate = 0;
    $scope.max = 5;
    $scope.isReadonly = false;

    $scope.hoveringOver = function (nota) {
        $scope.overStar = nota;
        if (nota < 2) {
            $scope.notaCss = "danger";
            $scope.notaTexto = "Péssimo";
        } else if (nota < 4) {
            $scope.notaCss = "warning";
            $scope.notaTexto = "Razoável";
        } else if (nota < 5) {
            $scope.notaCss = "info";
            $scope.notaTexto = "Bom";
        } else {
            $scope.notaCss = "success";
            $scope.notaTexto = "Ótimo";
        }
    };

    $scope.ok = function () {
        $modalInstance.close($scope.selected.item);
    };

    $rootScope.closeModalDesafios = function () {
        $modalInstance.dismiss();
    };

    $scope.mudarValorAuxiliar = function (variavel, valor) {
        $scope.aux[variavel] = valor;
        $scope.$apply();
    };

    rotacao = -180;
    setInterval(function () {
        if (rotacao == 0) {
            rotacao = -180;
        } else {
            rotacao = 0;
        }
        $(".vertical.flip-container .flipper").css("transform", "rotateX(" + rotacao + "deg)");
    }, 5000);
});
