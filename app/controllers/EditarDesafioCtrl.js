app.controller("EditarDesafioCtrl", function ($rootScope, $scope, $modal, $http, validaDesafio, $location) {
    $('#timepicker').timepicker({
        minuteStep: 1,
        template: 'modal',
        appendWidgetTo: 'body',
        showSeconds: true,
        showMeridian: false,
        defaultTime: false
    });
    $scope.editorOptions = {
        lineNumbers: true,
        styleActiveLine: true,
        matchBrackets: true,
        indentUnit: 4,
        autoMatchParens: true,
        parserConfig: { 'pythonVersion': 2, 'strictErrors': true },
        theme: 'eclipse',
    };

    $scope.desafio = [];
    $scope.entradas = [];
    $scope.saidas = [];
    $scope.ajudaItens = [];
    $scope.ajuda = false;
    $scope.desafio.cssClass = "default";
 
    $scope.setDificuldadeEditar = function (dificuldade) {
        $scope.Dificuldade = dificuldade;
        $scope.ajuda = true;
        $scope.desafio.recompensa = [];
        if (dificuldade == 1) {
            $scope.desafio.cssClass = $scope.ajudaClass = "success";
            $scope.ajudaMsg = "Nível voltado ao jogadores iniciantes. Neste nível os desafios devem ser simples e de fácil entendimento.";
            $scope.ajudaItens = [];
            $scope.ajudaItens.push("Declaração de variáveis (tipos primitivos)");
            $scope.ajudaItens.push("Entrada e saída de dados (funções de leitura e impressão)");
            $scope.ajudaItens.push("Instrução if...else (simples, sem encadeamento)");
            $scope.ajudaReqNivel = "Padawan";
            $scope.ajudaReqCreditos = 20;
            $scope.ajudaRecDf = 100;
            $scope.ajudaRecCreditos = 1;
            $scope.desafio.recompensa.creditos = 15;
            $scope.desafio.recompensa.df = 50;
        }
        if (dificuldade == 2) {
            $scope.desafio.cssClass = $scope.ajudaClass = "primary";
            $scope.ajudaMsg = "Este segundo nível ainda é considerado para jogadores iniciantes, mas já com uma certa experiência no jogo."
                + " Os desafios podem testar mais a interpretação do jogador.";
            $scope.ajudaItens = [];
            $scope.ajudaItens.push("Estruturas condicionais");
            $scope.ajudaItens.push("Operadores lógicos");
            $scope.ajudaItens.push("Operadores relacionais");
            $scope.ajudaItens.push("Instrução if...else (encadeados)");
            $scope.ajudaReqNivel = "Jedi";
            $scope.ajudaReqCreditos = 50;
            $scope.ajudaRecDf = 200;
            $scope.ajudaRecCreditos = 2;
            $scope.desafio.recompensa.creditos = 50;
            $scope.desafio.recompensa.df = 100;
        }
        if (dificuldade == 3) {
            $scope.desafio.cssClass = $scope.ajudaClass = "warning";
            $scope.ajudaMsg = "Nível intermediário, os desafios passam a ser mais completos e complexos.";
            $scope.ajudaItens = [];
            $scope.ajudaItens.push("Estruturas de repetição");
            $scope.ajudaItens.push("Contadores");
            $scope.ajudaItens.push("Acumuladores");
            $scope.ajudaItens.push("Instrução for e while");
            $scope.ajudaReqNivel = "Mestre Jedi";
            $scope.ajudaReqCreditos = 100;
            $scope.ajudaRecDf = 400;
            $scope.ajudaRecCreditos = 5;
            $scope.desafio.recompensa.creditos = 100;
            $scope.desafio.recompensa.df = 200;
        }
        if (dificuldade == 4) {
            $scope.desafio.cssClass = $scope.ajudaClass = "purple";
            $scope.ajudaMsg = "Nível mais alto de dificuldade do jogo. Logo, os desafios criados neste nível devem ser o mais difícil possível."
                + " Sendo um nível voltado para jogadores com alta experiência no jogo, é importante que haja enunciados desafiadores para os jogadores.";
            $scope.ajudaItens = [];
            $scope.ajudaItens.push("Laços encadeados");
            $scope.ajudaItens.push("Matrizes unidimensionais (vetores)");
            $scope.ajudaItens.push("Matrizes bidimensionais");
            $scope.ajudaReqNivel = "Mestre Jedi";
            $scope.ajudaReqCreditos = 200;
            $scope.ajudaRecDf = 1000;
            $scope.ajudaRecCreditos = 10;
            $scope.desafio.recompensa.creditos = 200;
            $scope.desafio.recompensa.df = 500;
        }

    };

    var getDesafio = function () {
        $http.get(host + "verifica-historico-desafio/" + $rootScope.idDesafio).then(
            function success(res) {
                if (res.data) {
                    $location.ath("/gerenciar-meus-desafios");
                    notification('aviso', '', 'O desafio não pode ser editado pois já foi comprado por alguém!');
                } else {
                    $http.get(host + "desafio/" + $rootScope.idDesafio).then(
                        function success(response) {
                            var tgs = organizaTags(response.data[0]);
                            $scope.desafio = response.data[0];
                            $scope.desafio.Tags = tgs;
                            $scope.desafio.Entradas = response.data[0].Entradas[0];
                            $scope.desafio.Saidas = response.data[0].Saidas[0];
                            $scope.setDificuldadeEditar(response.data[0].Dificuldade);
                        }
                    );
                }
            }, function error(err) {
                notification('erro', 'FALHA', 'Falha ao obter o desafio');
            }
        );
    }
    getDesafio();

    $scope.atualizarDesafio = function (desafio) {
        desafio = {
            Id: $scope.desafio.Id,
            Titulo: $scope.desafio.Titulo,
            Tempo: $scope.desafio.Tempo == undefined || $scope.desafio.Tempo == '' ? '00:00:00' : $scope.desafio.Tempo,
            Descricao: $scope.desafio.Descricao,
            Enunciado: $scope.desafio.Enunciado,
            PreCodigo: $scope.desafio.PreCodigo,
            Dificuldade: $scope.desafio.Dificuldade,
            Tags: $scope.desafio.Tags,
            Entradas: $scope.desafio.Entradas,
            Saidas: $scope.desafio.Saidas
        }
        if (desafio.Titulo != undefined && desafio.Titulo != '') {
            $("#titulo-group").removeClass('has-error');
            if (desafio.Tags != undefined && desafio.Tags != '') {
                $("#tags-group").removeClass('has-error');
                if (desafio.Entradas != undefined && desafio.Entradas.Valor != '') {
                    $("#entradas-desafio-group").removeClass('has-error');
                    if (desafio.Saidas != undefined && desafio.Saidas.Valor != '') {
                        $("#saidas-desafio-group").removeClass('has-error');
                        if (desafio.Dificuldade > 0) {
                            if (desafio.Descricao != undefined && desafio.Descricao != '') {
                                $("#descricao-group").removeClass('has-error');
                                if (desafio.Enunciado != undefined && desafio.Enunciado != '') {
                                    $("#enunciado-group").removeClass('has-error');
                                    if (validaDesafio.validaCriacaoDesafio(desafio.Dificuldade, $rootScope.usuarioLogado)) {
                                        $http.put(host + "desafio/" + desafio.Id, desafio).then(
                                            function success(res) {
                                                getDesafio();
                                                $location.path("/gerenciar-meus-desafios")
                                                notification("sucesso", "SUCESSO", "Desafio atualizado com sucesso");
                                            }, function error(err) {
                                                notification("erro", "FALHA", "Falha ao atualizar desafio");
                                            }
                                        );
                                    } else {
                                        notification('aviso', 'AVISO', 'Usuário não tem os requisitos para criar um desafio com esse nível de dificuldade!');
                                    }
                                } else {//Valida Enunciado
                                    $("#enunciado").focus();
                                    $("#enunciado-group").addClass('has-error');
                                    notification('aviso', 'AVISO', 'Insira o Enunciado!');
                                }
                            }
                            else {//Valida Descrição
                                $("#descricao").focus();
                                $("#descricao-group").addClass('has-error');
                                notification('aviso', 'AVISO', 'Insira uma Descrição!');
                            }
                        } else {//Valida Dificuldade
                            notification('aviso', 'AVISO', 'Informe qual o Nível de Dificuldade do desafio!');
                        }
                    } else {//Valida Saídas
                        $("#saidas-desafio").focus();
                        $("#saidas-desafio-group").addClass('has-error');
                        notification('aviso', 'AVISO', 'Informe pelo menos uma Saída!');
                    }
                } else {//Valida Entradas
                    $("#entradas-desafio").focus();
                    $("#entradas-desafio-group").addClass('has-error');
                    notification('aviso', 'AVISO', 'Informe pelo menos uma Entrada!');
                }
            } else {//Valida Tags
                $("#tags").focus();
                $("#tags-group").addClass('has-error');
                notification('aviso', 'AVISO', 'Insira a(s) Tag(s) para o desafio!');
            }
        }
        else {//Valida Título
            $("#titulo").focus();
            $("#titulo-group").addClass('has-error');
            notification('aviso', 'AVISO', 'Insira um Título para o desafio!');
        }
    }
});
