var tema = JSON.parse(localStorage.getItem("theme"));
var editor = CodeMirror.fromTextArea(document.getElementById("interpretadorPython"), {
    lineNumbers: true,
    styleActiveLine: true,
    matchBrackets: true,
    indentUnit: 4,
    autoMatchParens: true,
    height: 20,
    parserConfig: { 'pythonVersion': 2, 'strictErrors': true }
});

if (tema != null) {
    $('#selectBox option:eq(' + tema.index + ')').attr('selected', 'selected');
    editor.setOption("theme", tema.nome);
} else {
    var theme = { nome: 'default', index: 0 };
    localStorage.setItem("theme", JSON.stringify(theme));
    editor.setOption("theme", theme.nome);
}

$("#selectBox").change(function () {
    var theme = { nome: $("#selectBox").val(), index: $("#selectBox")[0].selectedIndex };
    localStorage.setItem("theme", JSON.stringify(theme));
    tema = JSON.parse(localStorage.getItem("theme"));
    if (tema != null) {
        editor.setOption("theme", tema.nome);
    }
});