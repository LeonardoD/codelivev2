function tipoDesafio(dificuldade) {
    var configDesafio = {};
    if (dificuldade == 1) {
        configDesafio = {
            classe: "success",
            classeJogador: "Novato",
            recompensas: {
                nivelDf: 100,
                creditos: 15,
                nivelDfCriacao: 100
            },
            requisitos: {
                classe: "Novato",
                nivel: 1,
                creditosCobrados: 5
            }
        };
        return configDesafio;
    }
    if (dificuldade == 2) {
        configDesafio = {
            classe: "primary",
            classeJogador: "Padawan",
            recompensas: {
                nivelDf: 100,
                creditos: 50,
                nivelDfCriacao: 200
            },
            requisitos: {
                classe: "Novato",
                nivel: 3,
                creditosCobrados: 15
            }
        };
        return configDesafio;
    }
    if (dificuldade == 3) {
        configDesafio = {
            classe: "warning",
            classeJogador: "Jedi",
            recompensas: {
                nivelDf: 200,
                creditos: 100,
                nivelDfCriacao: 400
            },
            requisitos: {
                classe: "Padawan",
                nivel: 1,
                creditosCobrados: 25
            }
        };
        return configDesafio;
    }
    if (dificuldade == 4) {
        configDesafio = {
            classe: "purple",
            classeJogador: "Mestre Jedi",
            recompensas: {
                nivelDf: 500,
                creditos: 200,
                nivelDfCriacao: 1000
            },
            requisitos: {
                classe: "Jedi",
                nivel: 1,
                creditosCobrados: 50
            }
        };
        return configDesafio;
    }
}