function notification(type, title, text) {
	if (type == "erro") {
		$.growl.error({ title: title, message: text });
	}
	if (type == "sucesso") {
		$.growl.notice({ title: title, message: text });
	}
	if (type == "aviso") {
		$.growl.warning({ title: title, message: text });
	} if (type == "info") {
		$.growl({ title: title, message: text });
	}
}