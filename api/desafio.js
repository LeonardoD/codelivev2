module.exports = function (app) {
    var db = app.get("db");
    var io = app.get("io");
    var q = app.get("q");
    var clients = app.get("clientsLogged");
    var DesafioCtrl = {
        getDesafios: function (req, res) {
            var usuariosDesafio = [];
            var saidasDesafio = [];
            var entradasDesafio = [];

            function getAllDesafios() {
                var deferred = q.defer()
                db.query('SELECT * FROM Desafio WHERE Publicado = true',
                    function (err, rows) {
                        if (err) throw err;
                        deferred.resolve(rows);
                    });
                return deferred.promise;
            }

            getAllDesafios().then(function (desafios) {
                desafios.forEach(function (desafio) {
                    saidasDesafio.push(getSaidasDesafio(desafio.Id)
                        .then(function (saidas) {
                            desafio.Saidas = saidas;
                            return desafio;
                        }));
                    entradasDesafio.push(getEntradasDesafio(desafio.Id)
                        .then(function (entradas) {
                            desafio.Entradas = entradas;
                            return desafio;
                        }));
                    usuariosDesafio.push(getAutorDesafio(desafio.Autor)
                        .then(function (usuario) {
                            desafio.Usuario = usuario;
                            return desafio;
                        }));
                });
                q.all(usuariosDesafio).then(function (desafios) {
                    res.json(desafios);
                });
            });
        },
        getDesafio: function (req, res) {
            var usuariosDesafio = [];
            var saidasDesafio = [];
            var entradasDesafio = [];
            function findDesafio(id) {
                var deferred = q.defer();
                db.query('SELECT * FROM Desafio WHERE Id = ?',
                    id, function (err, rows) {
                        if (err) throw err;
                        deferred.resolve(rows);
                    });
                return deferred.promise;
            }
            findDesafio(req.params.id).then(function (desafios) {
                desafios.forEach(function (desafio) {
                    saidasDesafio.push(getSaidasDesafio(desafio.Id)
                        .then(function (saidas) {
                            desafio.Saidas = saidas;
                            return desafio;
                        }));
                    entradasDesafio.push(getEntradasDesafio(desafio.Id)
                        .then(function (entradas) {
                            desafio.Entradas = entradas;
                            return desafio;
                        }));
                    usuariosDesafio.push(getAutorDesafio(desafio.Autor)
                        .then(function (usuario) {
                            desafio.Usuario = usuario;
                            return desafio;
                        }));
                });
                q.all(usuariosDesafio).then(function (desafio) {
                    res.json(desafio);
                });
            });
        },
        getDesafiosPorUsuario: function (req, res) {
            var saidasDesafio = [];
            var entradasDesafio = [];
            getDesafiosUsuario(req.params.id).then(function (desafios) {
                desafios.forEach(function (desafio) {
                    saidasDesafio.push(getSaidasDesafio(desafio.Id)
                        .then(function (saidas) {
                            desafio.Saidas = saidas;
                            return desafio;
                        }));
                    entradasDesafio.push(getEntradasDesafio(desafio.Id)
                        .then(function (entradas) {
                            desafio.Entradas = entradas;
                            return desafio;
                        }));
                });
                q.all(entradasDesafio).then(function (desafios) {
                    res.json(desafios);
                });
            });
        },
        updateDesafio: function (req, res) {
            var desafio = req.body;
            var tags = getTags(desafio);
            console.log(desafio);
            atualizarEntradas(desafio.Entradas, desafio.Id).then(function (result) {
                console.log("Entrada atualizada? " + result);
            });

            atualizarSaidas(desafio.Saidas, desafio.Id).then(function (result) {
                console.log("Saída atualizada? " + result);
            });

            db.query('UPDATE desafio SET Titulo = ?, Tags = ?, Tempo = ?, Descricao = ?, Enunciado = ?, PreCodigo =  ?, Dificuldade = ? WHERE Id = ?',
                [
                    desafio.Titulo,
                    tags,
                    desafio.Tempo,
                    desafio.Descricao,
                    desafio.Enunciado,
                    desafio.PreCodigo,
                    desafio.Dificuldade,
                    req.params.id
                ], function (err, rows) {
                    if (err) throw err;
                    res.setHeader('Access-Control-Allow-Origin', '*');
                    res.json(rows.affectedRows);
                });
        },
        ativarDesafio: function (req, res) {
            db.query('UPDATE Desafio d SET d.Publicado = ? WHERE  d.Id = ?',
                [
                    true,
                    req.params.id
                ], function (err, rows) {
                    if (err) throw err;
                    res.setHeader('Access-Control-Allow-Origin', '*');
                    res.json(rows);
                });
            io.emit('updateStatusDesafio');
            findUsuarioSocket(req.session.usuarioLogado.NomeExibicao).then(function (client) {
                client.conn.broadcast.emit('novoDesafio', "Novo desafio disponível!");
            });
        },
        desativarDesafio: function (req, res) {
            db.query('UPDATE Desafio d SET d.Publicado = ? WHERE  d.Id = ?',
                [
                    false,
                    req.params.id
                ], function (err, rows) {
                    if (err) throw err;
                    res.setHeader('Access-Control-Allow-Origin', '*');
                    res.json(rows);
                });
            io.emit('updateStatusDesafio');
        },
        setDesafio: function (req, res) {
            var desafio = req.body;
            var tags = getTags(desafio);
            db.query('INSERT INTO Desafio (Titulo, Tags, Tempo, Descricao, Enunciado, PreCodigo, Dificuldade, Autor, Publicado, DataCadastro) VALUES (?,?,?,?,?,?,?,?,?,?)',
                [
                    desafio.Titulo,
                    tags,
                    desafio.Tempo,
                    desafio.Descricao,
                    desafio.Enunciado,
                    desafio.PreCodigo,
                    desafio.Dificuldade,
                    desafio.Autor,
                    true,
                    new Date()
                ],
                function (err, rows) {
                    if (err) throw err;
                    addEntradas(desafio.Entradas, rows.insertId);
                    addSaidas(desafio.Saidas, rows.insertId);
                    atualizaDfAutor(desafio.Autor, desafio.Config.recompensas.nivelDfCriacao);
                    console.log("novo desafio!");
                });
            io.emit('novoDesafio', "Novo desafio disponível!");
            res.json(true);
        },
        comprarDesafio: function (req, res) {
            var desafio = req.body.desafio;
            getUsuario(desafio.Comprador.Id).then(function (usuarioComprador) {
                atualizaCreditoUsuario(usuarioComprador.Id, (usuarioComprador.Credito - desafio.Config.requisitos.creditosCobrados))
                    .then(function (value) {
                        console.log("Atulizada comprador! " + value);
                    });

                getUsuario(desafio.Autor).then(function (usuarioAutor) {
                    atualizaCreditoUsuario(desafio.Autor, (usuarioAutor.Credito + desafio.Config.requisitos.creditosCobrados))
                        .then(function (value) {
                            console.log("Atulizada autor! " + value);
                        });
                });

                getUsuario(desafio.Comprador.Id).then(function (usuario) {
                    req.session.usuarioLogado = usuario;
                    res.json(usuario);
                });
            });
        },
        setRespostaDesafio: function (req, res) {
            var deferred = q.defer();
            var desafio = req.body;
            setResposta(req.body).then(function (idRespota) {
                db.query('INSERT INTO HistoricoDesafio(IdResposta, DesafioVencido) VALUES (?, ?) ', [
                    idRespota,
                    desafio.DesafioCorreto
                ], function (err, rows) {
                    if (err) throw deferred.reject(err);
                    deferred.resolve(rows.affectedRows);
                });
                return deferred.promise;
            });
            if (desafio.DesafioCorreto == true) {
                updateRecompensasUsuario(desafio.Config.recompensas, desafio.Comprador)
                    .then(function (result) {
                        getUsuario(desafio.Comprador.Id)
                            .then(function (usuario) {
                                req.session.usuarioLogado = usuario;
                                res.json(usuario);
                            });
                    });
            }
            db.query('SELECT distinct d.Id as idDesafio, d.Dificuldade, hd.DesafioVencido, u.NomeExibicao FROM '
                + 'HistoricoDesafio hd '
                + 'INNER JOIN Resposta r ON hd.IdResposta = r.Id '
                + 'INNER JOIN Usuario u ON u.Id = r.IdUsuario '
                + 'INNER JOIN Desafio d ON d.Id = r.IdDesafio WHERE d.Id = ?', desafio.Id,
                function (err, rows) {
                    if (err) throw err;
                    if (rows.length > 0) {
                        rows.forEach(function (historico) {
                            findUsuarioSocket(historico.NomeExibicao).then(function (client) {
                                if (historico.NomeExibicao != desafio.Comprador.NomeExibicao) {
                                    client.conn.emit('meuDesafioRespondido', 'O usuário: '
                                        + desafio.Comprador.NomeExibicao + ' respondeu o desafio: '
                                        + desafio.Titulo + ' que você já participou!');
                                }
                            });
                        });
                    }
                });

            findUsuarioSocket(desafio.Usuario.NomeExibicao).then(function (client) {
                client.conn.emit('meuDesafioRespondido', 'O usuário: '
                    + desafio.Comprador.NomeExibicao + ' respondeu o desafio: '
                    + desafio.Titulo);
            });
            io.emit('atualizarRanking');
        },
        getHistoricoDesafioUsuario: function (req, res) {
            db.query('SELECT d.Id as idDesafio, d.Dificuldade, hd.DesafioVencido, u.NomeExibicao FROM '
                + 'HistoricoDesafio hd '
                + 'INNER JOIN Resposta r ON hd.IdResposta = r.Id '
                + 'INNER JOIN Usuario u ON u.Id = r.IdUsuario '
                + 'INNER JOIN Desafio d ON d.Id = r.IdDesafio WHERE u.Id = ?', req.params.id,
                function (err, rows) {
                    if (err) throw err;
                    return res.json(rows);
                });
        },
        getHistoricoDesafio: function (req, res) {
            db.query('SELECT d.Id as idDesafio, d.Dificuldade, hd.DesafioVencido, u.NomeExibicao FROM '
                + 'HistoricoDesafio hd '
                + 'INNER JOIN Resposta r ON hd.IdResposta = r.Id '
                + 'INNER JOIN Usuario u ON u.Id = r.IdUsuario '
                + 'INNER JOIN Desafio d ON d.Id = r.IdDesafio WHERE d.Id = ?', req.params.id,
                function (err, rows) {
                    if (err) throw err;
                    return res.json(rows);
                });
        },
        verificaHistoricoDesafio: function (req, res) {
            db.query('SELECT * FROM HistoricoDesafio hd INNER JOIN Resposta r ON hd.IdResposta = r.Id '
                + 'INNER JOIN Desafio d ON d.Id = r.IdDesafio WHERE d.Id = ?', req.params.id,
                function (err, rows) {
                    if (err) throw err;
                    return res.json(rows.length > 0);
                });
        },
        verificaDesafioVencido: function (req, res) {
            var dados = req.body.dados;
            db.query('SELECT * FROM HistoricoDesafio hd '
                + 'INNER JOIN Resposta r ON hd.IdResposta = r.Id '
                + 'INNER JOIN Desafio d ON d.Id = r.IdDesafio '
                + 'INNER JOIN Usuario u ON u.Id = r.IdUsuario '
                + 'WHERE hd.DesafioVencido = 1 AND d.Id = ? AND u.Id = ?', [
                    dados.idDesafio,
                    dados.idUsuario
                ],
                function (err, rows) {
                    if (err) throw err;
                    return res.json(rows.length > 0);
                });
        },
        getDesafiosComprados: function (req, res) {
            var listaDesafios = [];
            var listaRespostasTreino = [];
            var usuariosDesafio = [];

            function getHistoricoDesafioComprado(idDesafio, idUsuario) {
                var deferred = q.defer();
                db.query('SELECT d.Id as IdDesafio, hd.DesafioVencido, u.Id as IdUsuario, r.Resposta, r.DataResposta FROM HistoricoDesafio hd '
                    + 'INNER JOIN Resposta r ON hd.IdResposta = r.Id '
                    + 'INNER JOIN Desafio d ON d.Id = r.IdDesafio '
                    + 'INNER JOIN Usuario u ON u.Id = r.IdUsuario '
                    + 'WHERE d.Id = ? AND u.Id = ?', [idDesafio, idUsuario],
                    function (err, rows) {
                        if (err) throw deferred.reject(err);
                        deferred.resolve(rows);
                    });
                return deferred.promise;
            }

            function getDesafiosUsuario() {
                var deferred = q.defer();
                db.query('SELECT distinct d.Id as IdDesafio, u.Id as IdUsuario, d.Titulo, d.Tempo, d.Descricao, d.Enunciado, d.Autor, d.Dificuldade, d.PreCodigo '
                    + 'FROM Desafio d '
                    + 'INNER JOIN Resposta r ON d.Id = r.IdDesafio '
                    + 'INNER JOIN Usuario u ON  u.Id = r.IdUsuario WHERE u.Id = ?', req.params.id,
                    function (err, rows) {
                        if (err) throw deferred.reject(err);
                        deferred.resolve(rows);
                    });
                return deferred.promise;
            }

            function getRespostasTreino(idDesafio) {
                var deferred = q.defer();
                db.query('SELECT * FROM RespostaTreino WHERE IdDesafio = ?', idDesafio,
                    function (err, rows) {
                        if (err) throw deferred.reject(err);
                        deferred.resolve(rows);
                    });
                return deferred.promise;
            }

            getDesafiosUsuario().then(function (desafios) {
                desafios.forEach(function (desafio) {
                    listaDesafios.push(getHistoricoDesafioComprado(desafio.IdDesafio, desafio.IdUsuario)
                        .then(function (historicos) {
                            desafio.Historico = historicos;
                            return desafio;
                        }));
                    usuariosDesafio.push(getAutorDesafio(desafio.Autor)
                        .then(function (usuario) {
                            desafio.Usuario = usuario;
                            return desafio;
                        }));
                    listaRespostasTreino.push(getRespostasTreino(desafio.IdDesafio).then(function (respostas) {
                        desafio.RespostasTreino = respostas;
                        return desafio;
                    }));
                })
                q.all(listaRespostasTreino).then(function (desafios) {
                    res.json(desafios);
                });
            });
        }
    }

    function getAutorDesafio(idUsuario) {
        var deferred = q.defer()
        db.query('SELECT * FROM Imagem i INNER JOIN Usuario u ON i.Id = u.IdImagem WHERE u.Id = ?', idUsuario,
            function (err, rows) {
                if (err) throw err;
                deferred.resolve(rows[0]);
            });
        return deferred.promise;
    }

    function findUsuarioSocket(nomeExibicao) {
        var deferred = q.defer();
        clients.forEach(function (client) {
            if (client.name == nomeExibicao) {
                deferred.resolve(client);
            }
        });
        return deferred.promise;
    }

    function setResposta(desafio) {
        var deferred = q.defer();
        db.query('INSERT INTO Resposta(Resposta, IdDesafio, IdUsuario, DataResposta) VALUES (?,?,?,?) ', [
            desafio.Resposta,
            desafio.Id,
            desafio.Comprador.Id,
            new Date()
        ], function (err, rows) {
            if (err) throw deferred.reject(err);
            deferred.resolve(rows.insertId);
        });
        return deferred.promise;
    }

    function getDesafiosUsuario(idUsuario) {
        var deferred = q.defer();
        db.query('SELECT * FROM Desafio d WHERE d.Autor = ?', idUsuario,
            function (err, rows) {
                if (err) throw deferred.reject(err);
                deferred.resolve(rows);
            });
        return deferred.promise;
    }

    function getEntradasDesafio(idDesafio) {
        var deferred = q.defer();
        db.query('SELECT * FROM EntradasDesafio e WHERE e.IdDesafio = ?', idDesafio,
            function (err, rows) {
                if (err) throw err;
                deferred.resolve(rows);
            });
        return deferred.promise;
    }

    function getSaidasDesafio(idDesafio) {
        var deferred = q.defer();
        db.query('SELECT * FROM SaidasDesafio s WHERE s.IdDesafio = ?', idDesafio,
            function (err, rows) {
                if (err) throw err;
                deferred.resolve(rows);
            });
        return deferred.promise;
    }

    function getTags(desafio) {
        var tags = "";
        for (i = 0; i < desafio.Tags.length; i++) {
            if (i != (desafio.Tags.length - 1)) {
                tags += desafio.Tags[i].text + ",";
            } else {
                tags += desafio.Tags[i].text;
            }
        }
        return tags;
    }

    function addEntradas(entradas, idDesafio) {
        var deferred = q.defer();
        db.query("INSERT INTO EntradasDesafio (Valor, IdDesafio) VALUES (?,?)", [
            entradas,
            idDesafio
        ], function (err, rows) {
            if (err) throw err;
            deferred.resolve(rows.affectedRows);
        });
        return deferred.promise;
    }

    function atualizarEntradas(entradas, idDesafio) {
        var deferred = q.defer();
        if (entradas.Id > 0) {
            db.query("UPDATE EntradasDesafio SET Valor = ? WHERE Id = ?",
                [
                    entradas.Valor,
                    entradas.Id
                ], function (err, rows) {
                    if (err) throw err;
                    deferred.resolve(rows.affectedRows);
                });
        } else {
            deferred.resolve(addEntradas(entradas.Valor, idDesafio));
        }
        return deferred.promise;
    }

    function addSaidas(saidas, idDesafio) {
        var deferred = q.defer();
        db.query("INSERT INTO SaidasDesafio (Valor, IdDesafio) VALUES (?,?)", [
            saidas,
            idDesafio
        ], function (err, rows) {
            if (err) throw err;
            deferred.resolve(rows.affectedRows);
        }); deferred.promise;
    }

    function atualizarSaidas(saidas, idDesafio) {
        var deferred = q.defer();
        if (saidas.Id > 0) {
            db.query("UPDATE SaidasDesafio SET Valor = ? WHERE Id = ?",
                [
                    saidas.Valor,
                    saidas.Id
                ], function (err, rows) {
                    if (err) throw err;
                    deferred.resolve(rows.affectedRows);
                });
        } else {
            deferred.resolve(addSaidas(saidas.Valor, idDesafio));
        }
        return deferred.promise;
    }

    function atualizaDfAutor(autor, nivelDf) {
        var usuario = {};
        db.query("SELECT * FROM Usuario WHERE Id = ?", [autor],
            function (err, rows) {
                if (err) throw err;
                usuario = rows[0];
                if (usuario.Id != null) {
                    var novoNivelDf = (nivelDf + usuario.NivelDf);
                    db.query("UPDATE Usuario SET NivelDf = ? WHERE Id = ?", [
                        novoNivelDf,
                        usuario.Id
                    ], function (err, rows) {
                        if (err) throw err;
                        return rows.affectedRows;
                    });
                }
                return false;
            });
    }

    function getUsuario(idUsuario) {
        var deferred = q.defer();
        db.query('SELECT * FROM Imagem i INNER JOIN Usuario u ON i.Id = u.IdImagem WHERE u.Id = ?', idUsuario,
            function (err, rows) {
                if (err) throw err;
                deferred.resolve(rows[0]);
            });
        return deferred.promise;
    }

    function atualizaCreditoUsuario(idUsuario, credito) {
        var deferred = q.defer();
        db.query("UPDATE Usuario SET Credito = ? WHERE Id = ?",
            [
                credito,
                idUsuario
            ],
            function (err, rows) {
                if (err) throw err;
                deferred.resolve(rows.affectedRows);
            });
        return deferred.promise;
    }

    function setHistoricoDesafio(idUsuario, idDesafio) {
        var deferred = q.defer();
        db.query("INSERT INTO HistoricoDesafio (IdUsuario, IdDesafio) VALUES (?,?)",
            [
                idUsuario,
                idDesafio
            ],
            function (err, rows) {
                if (err) throw err;
                deferred.resolve(rows.affectedRows);
            });
        return deferred.promise;
    }

    function updateRecompensasUsuario(recompensas, comprador) {
        var deferred = q.defer();
        var novoCredito = recompensas.creditos + comprador.Credito
        var novoDf = recompensas.nivelDf + comprador.NivelDf;
        var evolucao = getEvolucao(novoDf);
        if (evolucao.Classe != comprador.Classe || evolucao.Nivel != comprador.Nivel) {
            novoCredito += evolucao.Credito;
            db.query('UPDATE Usuario SET Classe =?, Nivel = ?, NivelDf = ?, Credito = ? WHERE Id = ?', [
                evolucao.Classe,
                evolucao.Nivel,
                novoDf,
                novoCredito,
                comprador.Id
            ], function (err, rows) {
                if (err) throw deferred.reject(err);
                deferred.resolve(rows.affectedRows);
            });
        } else {
            db.query('UPDATE Usuario SET NivelDf = ?, Credito = ? WHERE Id = ?', [
                novoDf,
                novoCredito,
                comprador.Id
            ], function (err, rows) {
                if (err) throw deferred.reject(err);
                deferred.resolve(rows.affectedRows);
            });
        }
        return deferred.promise;
    }

    function getEvolucao(nivelDf) {
        // Mestre do Conselho
        if (nivelDf >= 30000) {
            evolucao = {
                Classe: 'Mestre do conselho',
                Nivel: 1,
                Credito: 2000
            }
            return evolucao;
        } else {
            //Novato
            if (nivelDf >= 0 && nivelDf <= 99) {
                evolucao = {
                    Classe: 'Novato',
                    Nivel: 1,
                    Credito: 50
                }
                return evolucao;
            }
            if (nivelDf >= 100 && nivelDf <= 249) {
                evolucao = {
                    Classe: 'Novato',
                    Nivel: 2,
                    Credito: 70
                }
                return evolucao;
            }
            if (nivelDf >= 250 && nivelDf <= 499) {
                evolucao = {
                    Classe: 'Novato',
                    Nivel: 3,
                    Credito: 100
                }
                return evolucao;
            }

            // Padawan	
            if (nivelDf >= 500 && nivelDf <= 799) {
                evolucao = {
                    Classe: 'Padawan',
                    Nivel: 1,
                    Credito: 120
                }
                return evolucao;
            }
            if (nivelDf >= 800 && nivelDf <= 1199) {
                evolucao = {
                    Classe: 'Padawan',
                    Nivel: 2,
                    Credito: 140
                }
                return evolucao;
            }
            if (nivelDf >= 1200 && nivelDf <= 1699) {
                evolucao = {
                    Classe: 'Padawan',
                    Nivel: 3,
                    Credito: 160
                }
                return evolucao;
            }
            if (nivelDf >= 1700 && nivelDf <= 2499) {
                evolucao = {
                    Classe: 'Padawan',
                    Nivel: 4,
                    Credito: 180
                }
                return evolucao;
            }
            if (nivelDf >= 2500 && nivelDf <= 3499) {
                evolucao = {
                    Classe: 'Padawan',
                    Nivel: 5,
                    Credito: 200
                }
                return evolucao;
            }

            // Cavaleiro Jedi
            if (nivelDf >= 3500 && nivelDf <= 4599) {
                evolucao = {
                    Classe: 'Cavaleiro Jedi',
                    Nivel: 1,
                    Credito: 200
                }
                return evolucao;
            }
            if (nivelDf >= 4600 && nivelDf <= 5799) {
                evolucao = {
                    Classe: 'Cavaleiro Jedi',
                    Nivel: 2,
                    Credito: 210
                }
                return evolucao;
            }
            if (nivelDf >= 5800 && nivelDf <= 7199) {
                evolucao = {
                    Classe: 'Cavaleiro Jedi',
                    Nivel: 3,
                    Credito: 220
                }
                return evolucao;
            }
            if (nivelDf >= 7200 && nivelDf <= 8799) {
                evolucao = {
                    Classe: 'Cavaleiro Jedi',
                    Nivel: 4,
                    Credito: 230
                }
                return evolucao;
            }
            if (nivelDf >= 8800 && nivelDf <= 10499) {
                evolucao = {
                    Classe: 'Cavaleiro Jedi',
                    Nivel: 5,
                    Credito: 240
                }
                return evolucao;
            }
            if (nivelDf >= 10500 && nivelDf <= 12399) {
                evolucao = {
                    Classe: 'Cavaleiro Jedi',
                    Nivel: 6,
                    Credito: 250
                }
                return evolucao;
            }
            if (nivelDf >= 12400 && nivelDf <= 14999) {
                evolucao = {
                    Classe: 'Cavaleiro Jedi',
                    Nivel: 7,
                    Credito: 300
                }
                return evolucao;
            }

            // Mestre Jedi	
            if (nivelDf >= 15000 && nivelDf <= 17999) {
                evolucao = {
                    Classe: 'Mestre Jedi',
                    Nivel: 1,
                    Credito: 500
                }
                return evolucao;
            }
            if (nivelDf >= 18000 && nivelDf <= 23999) {
                evolucao = {
                    Classe: 'Mestre Jedi',
                    Nivel: 2,
                    Credito: 700
                }
                return evolucao;
            }
            if (nivelDf >= 24000 && nivelDf <= 29999) {
                evolucao = {
                    Classe: 'Mestre Jedi',
                    Nivel: 3,
                    Credito: 1000
                }
                return evolucao;
            }
        }
    }
    return DesafioCtrl;
}