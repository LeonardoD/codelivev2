module.exports = function(app) {
    var db = app.get("db");
    var q = app.get("q");
    var nodemailer = app.get("nodemailer");
    var UsuarioCtrl = {
        getUsuarios: function(req, res) {
            db.query('SELECT * FROM Imagem i INNER JOIN Usuario u ON i.Id = u.IdImagem', function(err, rows) {
                if (err) throw err;
                res.json(rows);
            });
        },
        getUsuario: function(req, res) {
            getUsuario(req.params.id).then(function(usuario) {
                res.json(usuario);
            });
        },
        setUsuario: function(req, res) {
            db.query('INSERT INTO Usuario (Nome, Email, NomeExibicao, Senha, Nivel, NivelDf, Credito, IdImagem, Classe, Tipo)	VALUES (?,?,?,SHA1(?),?,?,?,?,?,?)', [
                    req.body.usuario.Nome,
                    req.body.usuario.Email,
                    req.body.usuario.NomeExibicao,
                    req.body.usuario.Senha,
                    1,
                    0,
                    50,
                    req.body.usuario.IdImagem,
                    "Novato",
                    "Aluno",
                ],
                function(err, rows) {
                    if (err) throw err;
                    res.json(rows.affectedRows);
                });
        },
        updateUsuario: function(req, res) {
            var usuario = req.body.usuario;

            function update() {
                var deferred = q.defer();
                db.query('UPDATE Usuario SET Nome = ?, Email = ?, NomeExibicao = ? WHERE Id = ?', [
                        usuario.Nome,
                        usuario.Email,
                        usuario.NomeExibicao,
                        req.params.id
                    ],
                    function(err, rows) {
                        if (err) throw deferred.reject(err);;
                        deferred.resolve(rows.affectedRows);
                    });
                return deferred.promise;
            }
            update().then(function(data) {
                if (data > 0) {
                    db.query('SELECT * FROM Imagem i INNER JOIN Usuario u ON u.IdImagem = i.Id WHERE u.Id = ?', req.params.id,
                        function(err, rows) {
                            req.session.usuarioLogado = rows[0];
                            res.json(req.session.usuarioLogado);
                        });
                }
            });
        },
        deleteUsuario: function(req, res) {
            db.query('DELETE FROM Usuario WHERE Id = ?', [req.params.id],
                function(err, rows) {
                    if (err) throw err;
                    res.json(rows.affectedRows);
                });
        },
        updateSenhaUsuario: function(req, res) {
            db.query('UPDATE Usuario SET Senha = sha1(?) WHERE Id = ?', [
                    req.body.usuario.NovaSenha,
                    req.params.id
                ],
                function(err, rows) {
                    if (err) throw err;
                    res.json(rows.affectedRows);
                });
        },
        login: function(req, res) {
            db.query('SELECT * FROM Imagem i INNER JOIN Usuario u ON u.IdImagem = i.Id AND u.NomeExibicao = ? AND u.Senha = sha1(?)', [
                    req.body.usuario.NomeExibicao,
                    req.body.usuario.Senha
                ],
                function(err, rows) {
                    if (err) {
                        throw err;
                    } else {
                        req.session.usuarioLogado = rows[0];
                        res.json(req.session.usuarioLogado);
                    }
                });
        },
        logoff: function(req, res) {
            req.session.usuarioLogado = null;
            res.json(true);
        },
        getUsuarioLogado: function(req, res) {
            if (req.session.usuarioLogado) {
                res.json(req.session.usuarioLogado);
            } else {
                res.json("");
            }
        },
        uploadImagem: function(req, res) {
            var newPath = req.file.path.replace('app', '');
            if (req.session.usuarioLogado) {
                var usuarioLogado = req.session.usuarioLogado;
                db.query('INSERT INTO Imagem (Descricao, Endereco) VALUES (?,?)', [
                        'avatar-usuario',
                        newPath
                    ],
                    function(err, rows) {
                        if (err) {
                            throw err;
                        } else {
                            atualizarImgUsuario(rows.insertId, usuarioLogado.Id, req)
                                .then(function(data) {
                                    res.json(data);
                                });
                        }
                    });
            }
        },
        getAvatar: function(req, res) {
            db.query('SELECT * FROM Imagem WHERE Descricao = ?', ['avatar'],
                function(err, rows) {
                    if (err) {
                        throw err;
                    } else {
                        res.json(rows);
                    }
                });
        },
        getConquistaUsuario: function(req, res) {
            db.query('SELECT * FROM Conquista WHERE IdUsuario = ?', [req.params.id],
                function(err, rows) {
                    if (err) {
                        throw err;
                    } else {
                        res.json(rows);
                    }
                });
        },
        setConquistaUsuario: function(req, res) {
            var usuario = req.body.usuario;
            verificaExistenciaConquista(usuario.Id, usuario.Conquista).then(function(data) {
                if (data.qtd == 0) {
                    db.query('INSERT INTO Conquista (Nome, IdUsuario) VALUES (?, ?)', [
                            usuario.Conquista,
                            usuario.Id
                        ],
                        function(err, rows) {
                            if (err) {
                                throw err;
                            } else {
                                res.json(rows);
                            }
                        });
                }
                console.log('update award', result.qtd);
            });
        },
        updateCreditoUsuario: function(req, res) {
            var usuario = req.body;
            db.query('UPDATE Usuario SET Credito = ? WHERE Id = ?', [
                    (usuario.Credito += 1),
                    usuario.Id
                ],
                function(err, rows) {
                    if (err) throw err;
                    getUsuario(usuario.Id).then(function(usuario) {
                        req.session.usuarioLogado = usuario;
                        res.json(usuario);
                    });
                });
        },
        setRespostaTreino: function(req, res) {
            var resposta = req.body;
            db.query('INSERT INTO RespostaTreino (IdUsuario, IdDesafio, Resposta, DataResposta) VALUES (?,?,?,?)', [
                    resposta.IdUsuario,
                    resposta.IdDesafio,
                    resposta.Resposta,
                    new Date()
                ],
                function(err, rows) {
                    if (err) throw err;
                    return res.json(rows.affectedRows);
                });
        },
        sendEmail: function(req, res) {
            var transporter = nodemailer.createTransport('smtps://jesielll3000@gmail.com:nos55555@smtp.gmail.com');
            var mailOptions = {
                from: '"Jesiel Padilha" <jesielll3000@gmail.com>',
                to: 'jesielpadilha.ti@gmail.com',
                subject: 'CodeLive - Solicitação de recuperação de senha',
                html: '<h2>Sua nova senha é: <b>' + req.body.usuario.NovaSenha + '</b> </h2>' +
                    '<p>Ao acessar a aplicação faça a alteração!</p>'
            };
            transporter.sendMail(mailOptions, function(error, info) {
                if (error) {
                    res.json(0);
                    return console.log(error);
                }
                console.log('Message sent: ' + info.response);
                res.json(1);
            });
        }
    };

    function atualizarImgUsuario(idImg, idUsuario, req) {
        var deferred = q.defer();
        if (idImg != null && idUsuario != null) {
            db.query('UPDATE Usuario SET IdImagem = ? WHERE Id = ?', [
                    idImg,
                    idUsuario
                ],
                function(err, rows) {
                    if (err) {
                        deferred.reject(false);
                        throw err;
                    } else {
                        var usuarioLogado = req.session.usuarioLogado;
                        getUsuario(usuarioLogado.Id).then(function(usuario) {
                            req.session.usuarioLogado = usuario;
                            deferred.resolve(usuario);
                        });
                    }
                });
        }
        return deferred.promise;
    }

    function getUsuario(idUsuario) {
        var deferred = q.defer();
        db.query('SELECT * FROM Imagem i INNER JOIN Usuario u ON i.Id = u.IdImagem WHERE u.Id = ?', idUsuario,
            function(err, rows) {
                if (err) throw err;
                deferred.resolve(rows[0]);
            });
        return deferred.promise;
    }

    function verificaExistenciaConquista(idUsuario, nomeConquista) {
        var deferred = q.defer();
        db.query('SELECT COUNT(c.Nome)  as qtd FROM Conquista c INNER JOIN Usuario u ON c.IdUsuario = u.Id WHERE c.Nome = ? AND u.Id = ?', [
                nomeConquista,
                idUsuario
            ],
            function(err, rows) {
                if (err) throw err;
                deferred.resolve(rows[0]);
            });
        return deferred.promise;
    }
    return UsuarioCtrl;
}