-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 07-Dez-2016 às 16:51
-- Versão do servidor: 10.1.10-MariaDB
-- PHP Version: 7.0.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `CodeLive2`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `AvaliacaoDesafio`
--

CREATE TABLE `AvaliacaoDesafio` (
  `Id` int(11) NOT NULL,
  `Comentario` text,
  `Nota` int(11) DEFAULT NULL,
  `DataHora` datetime DEFAULT NULL,
  `IdUsuario` int(11) NOT NULL,
  `IdDesafio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Conquista`
--

CREATE TABLE `Conquista` (
  `Id` int(11) NOT NULL,
  `Nome` varchar(50) DEFAULT NULL,
  `IdUsuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Desafio`
--

CREATE TABLE `Desafio` (
  `Id` int(11) NOT NULL,
  `Titulo` varchar(45) DEFAULT NULL,
  `Tags` text,
  `Tempo` time DEFAULT NULL,
  `Descricao` varchar(255) DEFAULT NULL,
  `Enunciado` text,
  `PreCodigo` text,
  `Publicado` tinyint(1) DEFAULT NULL COMMENT 'Campo quando "true", indica que o Desafio está publicado e pode ser visualizado por todos os usuários.',
  `Autor` int(11) NOT NULL COMMENT 'Usuario que criou o Desafio. Importante lembrar que o criador do Desafio não pode participar do mesmo.',
  `Dificuldade` int(11) DEFAULT NULL COMMENT 'Este campo indica que apenas um usuário com determinado NivelDf Poderá participar do Desafio. e.g: NivelDificuldade = 2, significa que os usuários NivelDf 1(novatos) não Poderão participar desse Desafio.',
  `DataCadastro` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `Desafio`
--

INSERT INTO `Desafio` (`Id`, `Titulo`, `Tags`, `Tempo`, `Descricao`, `Enunciado`, `PreCodigo`, `Publicado`, `Autor`, `Dificuldade`, `DataCadastro`) VALUES
(1, 'Simulado 1', 'Lógica', '00:00:00', 'A média aritmética de uma série de elementos é a soma de todos os termos e a posterior divisão pelo número de termos da série (por exemplo, se há 4 termos, some todos e divida o resultado por quatro).', '<p style="text-align: justify; ">A média aritmética de uma série de elementos é a soma de todos os termos e a posterior</p><p style="text-align: justify;">divisão pelo número de termos da série (por exemplo, se há 4 termos, some todos e&nbsp;</p><p style="text-align: justify;">divida o resultado por quatro).</p><p style="text-align: justify;">Para se calcular a dispersão da série em relação à média aritmética, utiliza-se&nbsp;</p><p style="text-align: justify;">a raiz quadrática da média quadrática das diferenças de cada valor da amostra em&nbsp;</p><p style="text-align: justify;">relação à média aritmética (por exemplo, tomemos uma amostra com quatro elementos:</p><p style="text-align: justify;">pegue o elemento 1, subtraia a média, eleve o resultado ao quadrado, divida por 4.</p><p style="text-align: justify;">Agora realize o mesmo procedimento para o elemento 2, 3 e 4, e some os resultados.</p><p style="text-align: justify;">Após somar os resultados, teremos a variância populacional da série de valores&nbsp;</p><p style="text-align: justify;">analisada. A raiz quadrada desse valor é denominada desvio padrão.</p><p style="text-align: justify;"><br></p><p style="text-align: justify;"><b>Entrada</b></p><p style="text-align: justify;">Faça um programa que leia 4 números inteiros, que constituem a série, sendo um&nbsp;</p><p style="text-align: justify;">número em cada linha (o usuário digita um número e salta uma linha, digita outro e</p><p style="text-align: justify;">salta, e assim por diante).</p><p style="text-align: justify;"><br></p><p style="text-align: justify;"><b>Saída</b></p><p style="text-align: justify;">O programa deve imprimir na tela a Média aritmética e o desvio padrão da série, ambos</p><p style="text-align: justify;">com uma precisão de 4 casas decimais. (Obs.: Um valor em cada linha).</p><p style="text-align: justify;"><br></p><p style="text-align: justify;">=================================================================================================</p><p style="text-align: justify;">Observações:</p><p style="text-align: justify;"><br></p><p style="text-align: justify;">1. Para calcular a raiz quadrada de um valor x, utilize a funcao math.sqrt(x);</p><p style="text-align: justify;">para isso voce deve acrescentar a linha:</p><p style="text-align: justify;"><br></p><p style="text-align: justify;"><i>import math&nbsp;</i></p><p style="text-align: justify;"><br></p><p style="text-align: justify;">no início do seu programa.</p><p style="text-align: justify;"><br></p><p style="text-align: justify;">2. Seu programa a ser enviado DEVE ser "suaMatricula.py", e DEVE ser zipado antes&nbsp;</p><p style="text-align: justify;">de mandar pelo Moodle, onde suaMatricula é o número de sua matricula, sem a barra.</p><p style="text-align: justify;">Por exemplo, 140000001.py e 140000001.zip</p><p style="text-align: justify;"><br></p><p style="text-align: justify;">3. Para formatar o número de casas decimais use %1.nf, onde n é o número de casas decimais desejado.</p><p style="text-align: justify;"><br></p><p style="text-align: justify;">=================================================================================================</p><p style="text-align: justify;"><b>Exemplo:</b><br></p><p style="text-align: justify;"><b>Entrada</b></p><p style="text-align: justify;">2&nbsp;</p><p style="text-align: justify;">5&nbsp;</p><p style="text-align: justify;">4&nbsp;</p><p style="text-align: justify;">1</p><p style="text-align: justify;"><br></p><p style="text-align: justify;"><b>Saída</b></p><p style="text-align: justify;">3.0000</p><p style="text-align: justify;">1.4142</p>', 'import math\n\nx1 = int(input())\nx2 = int(input())\nx3 = int(input())\nx4 = int(input())\n\n# Cálculo da Média Aritmética\nmedia = (x1 + x2 + x3 + x4)/4\n\n# Cálculo do Desvio Padrão\ndesvio = math.sqrt(((x1-media)**2 + (x2-media)*(x2-media) + (x3-media)**2 + (x4-media)**2)/4)\n\n#Saída\nprint("%1.4f" % media)\nprint("%1.4f" % desvio)', 1, 1, 1, '2016-12-07 11:25:46'),
(2, 'Simulado 2', 'Lógica', '00:00:00', 'Uma loja de informática está promovendo a venda de 2 produtos. Faça um programa que leia cinco números inteiros a respeito de uma compra de um desses produtos.', '<p>Uma loja de informática está promovendo a venda de 2 produtos:</p><p>laptops a 1500,00 reais cada, e ipads a 1000,00 reais cada.</p><p><br></p><p>A loja está fazendo várias promoções. A primeira delas é:</p><p><b>1) se o cliente comprar 3 ou mais unidades, entre laptops e ipads, a loja desconta 500 reais.</b></p><p><br></p><p>A segunda é:</p><p><b>2)sobre o valor resultante depois da primeira promoção, a loja dá um desconto ou acréscimo que depende da forma de pagamento</b>:</p><p><br></p><p><b>Se o cliente pagar a vista:</b></p><p>a) entre os dias 1 e 15, o desconto será de 10%<br></p><p>b) entre os dias 16 e 31, o desconto será de 5%.</p><p><br></p><p><b>Se o cliente pagar a prazo:</b></p><p>a) entre os dias 1 e 15, o acréscimo será de 8%</p><p>b) entre os dias 16 e 31, o acréscimo será de 10%.</p><p><br></p><p>Sobre o valor resultante depois das promoções, será cobrado o frete, que depende da distância da entrega:</p><p><b>a)</b> se a distância for menor ou igual a 50 km, o frete será de 100 reais;</p><p><b>b)</b> se a distância for maior que 50 km, o frete será de 200 reais.</p><p><br></p><p><b>Entrada</b></p><h4>Faça um programa que leia cinco números inteiros a respeito de<br>uma compra, nessa ordem: a quantidade de laptops, a quantidade<br>de ipads, o dia do pagamento, um valor que indica como será&nbsp;<br>efetuado o pagamento:</h4><p>0 para pagamento a vista;</p><p>1 para pagamento a prazo;</p><p>e a distância para a entrega.</p><p><br></p><p><b>Saída</b></p><p>O programa deve escrever na tela os seguintes valores reais:</p><p>a) valor da compra antes da aplicação das promoções;<br></p><p>b) valor depois da aplicação da primeira promoção;</p><p>c) valor depois da segunda promoção;espúrio</p><p>d) valor depois do cálculo do frete.</p><p><br></p><p><b>Os valores devem ser escritos nessa ordem, com a precisão de duas casas decimais, e devem estar um</b></p><p><b>em cada linha.</b></p><p>=================================================================================================</p><p><b>Obs.</b></p><p>1. O exemplo abaixo serve apenas para que o aluno verifique se entendeu o enunciado.</p><p>Para verificar o bom funcionamento do programa, o aluno deve testar o programa</p><p>com várias entradas diferentes, simulando todas as possibilidades que podem</p><p>ocorrer.&nbsp;</p><p>=================================================================================================<br></p><p><b>Exemplo</b><br></p><p><b>Entradas:</b></p><p>2&nbsp;</p><p>4&nbsp;</p><p>17&nbsp;</p><p>1&nbsp;</p><p>200</p><p><br></p><p><b>Saída:</b></p><p>7000.000000</p><p>6500.000000</p><p>7150.000000</p><p>7350.000000</p>', 'lap = int(input())\nipa = int(input())\ndia = int(input())\npag = int(input())\ndis = int(input())\n\nvalor1 = lap * 1500.00 + ipa * 1000.00\n\nif ((lap + ipa) >= 3):\n        valor2 = valor1 - 500\nelse:\n        valor2 = valor1\n\nif (pag == 0):\n        if (dia < 16):\n                valor3 = valor2 * 0.90\n        else:\n                valor3 = valor2 * 0.95\nelse:\n        if (dia < 16):\n                valor3 = valor2 * 1.08\n        else:\n                valor3 = valor2 * 1.10\n\nif (dis<=50):\n        valor4 = valor3 + 100\nelse:\n        valor4 = valor3 + 200\n\nprint("%1.2f" % valor1)\nprint("%1.2f" % valor2)\nprint("%1.2f" % valor3)\nprint("%1.2f" % valor4)\n\n', 1, 1, 2, '2016-12-07 11:26:56'),
(3, 'Simulado 3', 'Lógica', '00:00:00', 'O maior número que divide dois ou mais números naturais simultaneamente é chamado de máximo divisor comum, o mdc. Implemente uma função recursiva para calcular o mdc utilizando o algoritmo de divisões sucessivas.', '<p>O maior número que divide dois ou mais números naturais simultaneamente é chamado de máximo divisor comum, o mdc.<br></p><p>Implemente uma função recursiva para calcular o mdc utilizando o algoritmo de divisões sucessivas, também conhecido</p><p>como algoritmo de Euclides, a seguir resumido:</p><p><b><br></b></p><p><b>Algoritmo de Euclides:&nbsp;</b><br></p><p>Divide-se o número maior pelo menor. Se a divisão for exata, o mdc será o menor deles. Se a divisão não for exata, divide-se o menor pelo resto e assim sucessivamente, até encontrar uma divisão exata (resto zero). O último divisor será o mdc.</p><p><br></p><h4>Deseja-se saber o mdc entre os dois números e a quantidade de vezes que a função recursiva é chamada.</h4><p><br></p><p><b>Entrada:<br></b></p><p>Dois números inteiros, um em cada linha.</p><p><br></p><p><b>Saída:</b></p><p>O mdc na primeira linha.</p><p>A quantidade de vezes que a função recursiva foi chamada na segunda linha.</p><p>=============<br></p><p><b>Observações:</b></p><p>1. O exemplo abaixo serve apenas para que o aluno verifique se entendeu o enunciado.</p><p>Para verificar o bom funcionamento do programa, o aluno deve testar o programa</p><p>com várias sequências diferentes. O Corretor Automático utilizará sequências diferentes&nbsp;</p><p>da sequência do exemplo, na correção do programa.</p><p><br></p><p>=============</p><p><b>Exemplo 2:</b></p><p><b>Entrada:</b></p><p>200</p><p>144</p><p><b>Saída:</b><br></p><p>8</p><p>5</p><p><br></p><p><b>Exemplo 2:</b></p><p><b>Entrada</b>:</p><p>7907</p><p>2243</p><p><b>Saída:</b><br></p><p>1</p><p>10</p><p><br></p><p><br></p><p><b>Exemplo 3:</b></p><p><b>Entrada:</b><br></p><p>830069655965</p><p>93655941462188325</p><p><b>Saída:</b><br></p><p>4963255</p><p>8</p>', 'rec=0\n\ndef mdc(a, b):\n    global rec\n    rec +=1\n    resultado = a % b\n    if resultado == 0:\n        return b\n    else:\n        return mdc(b, resultado)\n\na=int(input())\nb=int(input())\n\nif a>=b:\n    x=mdc(a, b)\nelse:\n    x=mdc(b, a)\n\nprint(x)\nprint(rec)', 1, 1, 1, '2016-12-07 11:30:32');

-- --------------------------------------------------------

--
-- Estrutura da tabela `EntradasDesafio`
--

CREATE TABLE `EntradasDesafio` (
  `Id` int(11) NOT NULL,
  `Valor` text,
  `IdDesafio` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `EntradasDesafio`
--

INSERT INTO `EntradasDesafio` (`Id`, `Valor`, `IdDesafio`) VALUES
(1, '1\n7\n0\n-2', 1),
(2, '1 \n1 \n5 \n0 \n30', 2),
(3, '1333310\n2980185', 3);

-- --------------------------------------------------------

--
-- Estrutura da tabela `HistoricoDesafio`
--

CREATE TABLE `HistoricoDesafio` (
  `Id` int(11) NOT NULL,
  `IdResposta` int(11) NOT NULL,
  `DesafioVencido` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Imagem`
--

CREATE TABLE `Imagem` (
  `Id` int(11) NOT NULL,
  `Endereco` varchar(255) DEFAULT NULL,
  `Descricao` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `Imagem`
--

INSERT INTO `Imagem` (`Id`, `Endereco`, `Descricao`) VALUES
(1, '\\assets\\img\\personagens\\default.png', 'default'),
(2, '\\assets\\img\\personagens\\anakin.jpg', 'avatar'),
(3, '\\assets\\img\\personagens\\ioda.jpg', 'avatar'),
(4, '\\assets\\img\\personagens\\obi-wan.jpg', 'avatar'),
(5, '\\assets\\img\\personagens\\padawan.jpg', 'avatar'),
(6, '\\assets\\img\\personagens\\palpatine.jpg', 'avatar'),
(7, 'assets/img/personagens/darth-vader.jpg', 'avatar'),
(8, '\\assets\\img\\fotosUpload\\4565445-goku_by_maffo1989-d4vxux4-1475153991992.png', 'avatar-usuario'),
(9, '\\assets\\img\\fotosUpload\\john_lennon_by_luceene_k-d7ns7bt-1481125762560.jpg', 'avatar-usuario');

-- --------------------------------------------------------

--
-- Estrutura da tabela `Resposta`
--

CREATE TABLE `Resposta` (
  `Id` int(11) NOT NULL,
  `Resposta` text COLLATE utf8_unicode_ci,
  `IdDesafio` int(11) DEFAULT NULL,
  `IdUsuario` int(11) DEFAULT NULL,
  `DataResposta` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `RespostaTreino`
--

CREATE TABLE `RespostaTreino` (
  `Id` int(11) NOT NULL,
  `IdUsuario` int(11) NOT NULL,
  `IdDesafio` int(11) NOT NULL,
  `Resposta` text COLLATE utf8_unicode_ci NOT NULL,
  `DataResposta` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `SaidasDesafio`
--

CREATE TABLE `SaidasDesafio` (
  `Id` int(11) NOT NULL,
  `Valor` text,
  `IdDesafio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `SaidasDesafio`
--

INSERT INTO `SaidasDesafio` (`Id`, `Valor`, `IdDesafio`) VALUES
(1, '1.0000\n3.3166', 1),
(2, '2500.000000\n2500.000000\n2250.000000\n2350.000000', 2),
(3, '2635\n5', 3);

-- --------------------------------------------------------

--
-- Estrutura da tabela `Usuario`
--

CREATE TABLE `Usuario` (
  `Id` int(11) NOT NULL,
  `Nome` varchar(150) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `Senha` varchar(255) DEFAULT NULL,
  `Nivel` int(11) DEFAULT NULL COMMENT 'é uma pontuação numerica que indica o quão avançado está o usuário no jogo dentro de uma determinado classe.',
  `IdImagem` int(11) DEFAULT NULL,
  `NivelDf` bigint(20) DEFAULT NULL COMMENT 'e.g: novato, cavaleiro jedi, padawan, etc.',
  `Credito` int(11) DEFAULT NULL,
  `NomeExibicao` varchar(100) DEFAULT NULL,
  `Classe` varchar(50) DEFAULT NULL COMMENT 'Classe que o usuário de acordo com o NíveDf que ele possui. e.g: Novato, Padawan, Cavaleiro Jedi ou Mestre Jedi.',
  `DataCadastro` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `Usuario`
--

INSERT INTO `Usuario` (`Id`, `Nome`, `Email`, `Senha`, `Nivel`, `IdImagem`, `NivelDf`, `Credito`, `NomeExibicao`, `Classe`, `DataCadastro`) VALUES
(1, 'Jesiel S. Padilha', 'jesielpadilha.ti@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 1, 8, 32900, 50445, 'jesiel.padilha', 'Mestre do conselho', '2015-09-30 17:33:54'),
(2, 'yoda', 'yoda@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 1, 3, 0, 50, 'mestre yoda', 'Novato', '2016-12-07 11:53:59'),
(3, 'Anakin Skywalker', 'anakin@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 1, 2, 0, 50, 'anakin skywalker', 'Novato', '2016-12-07 12:48:05'),
(4, 'John Lennon', 'john@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 1, 9, 0, 50, 'john lennon', 'Novato', '2016-12-07 12:48:40');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `AvaliacaoDesafio`
--
ALTER TABLE `AvaliacaoDesafio`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Id_UNIQUE` (`Id`),
  ADD KEY `fk_Comentario_Usuario1_idx` (`IdUsuario`),
  ADD KEY `fk_Comentario_Desafio1_idx` (`IdDesafio`);

--
-- Indexes for table `Conquista`
--
ALTER TABLE `Conquista`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Id_UNIQUE` (`Id`),
  ADD KEY `fk_Conquista_Usuario1_idx` (`IdUsuario`);

--
-- Indexes for table `Desafio`
--
ALTER TABLE `Desafio`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `fk_Desafio_Usuario1_idx` (`Autor`);

--
-- Indexes for table `EntradasDesafio`
--
ALTER TABLE `EntradasDesafio`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IdDesafio` (`IdDesafio`);

--
-- Indexes for table `HistoricoDesafio`
--
ALTER TABLE `HistoricoDesafio`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Id_UNIQUE` (`Id`),
  ADD KEY `fk_HistorioDesafio_Usuario1_idx` (`IdResposta`);

--
-- Indexes for table `Imagem`
--
ALTER TABLE `Imagem`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `idImagem_UNIQUE` (`Id`);

--
-- Indexes for table `Resposta`
--
ALTER TABLE `Resposta`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IdDesafio` (`IdDesafio`),
  ADD KEY `IdUsuario` (`IdUsuario`);

--
-- Indexes for table `RespostaTreino`
--
ALTER TABLE `RespostaTreino`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IdUsuario` (`IdUsuario`),
  ADD KEY `IdDesafio` (`IdDesafio`);

--
-- Indexes for table `SaidasDesafio`
--
ALTER TABLE `SaidasDesafio`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Id_UNIQUE` (`Id`),
  ADD KEY `fk_SaidasDesafio_Desafio1_idx` (`IdDesafio`);

--
-- Indexes for table `Usuario`
--
ALTER TABLE `Usuario`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Id_UNIQUE` (`Id`),
  ADD KEY `fk_Usuario_Imagem1_idx` (`IdImagem`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Conquista`
--
ALTER TABLE `Conquista`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Desafio`
--
ALTER TABLE `Desafio`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `EntradasDesafio`
--
ALTER TABLE `EntradasDesafio`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `HistoricoDesafio`
--
ALTER TABLE `HistoricoDesafio`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Imagem`
--
ALTER TABLE `Imagem`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `Resposta`
--
ALTER TABLE `Resposta`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `RespostaTreino`
--
ALTER TABLE `RespostaTreino`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `SaidasDesafio`
--
ALTER TABLE `SaidasDesafio`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `Usuario`
--
ALTER TABLE `Usuario`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `AvaliacaoDesafio`
--
ALTER TABLE `AvaliacaoDesafio`
  ADD CONSTRAINT `fk_Comentario_Desafio1` FOREIGN KEY (`IdDesafio`) REFERENCES `Desafio` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_Comentario_Usuario1` FOREIGN KEY (`IdUsuario`) REFERENCES `Usuario` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `Conquista`
--
ALTER TABLE `Conquista`
  ADD CONSTRAINT `fk_Conquista_Usuario1` FOREIGN KEY (`IdUsuario`) REFERENCES `Usuario` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `Desafio`
--
ALTER TABLE `Desafio`
  ADD CONSTRAINT `fk_Desafio_Usuario1` FOREIGN KEY (`Autor`) REFERENCES `Usuario` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `EntradasDesafio`
--
ALTER TABLE `EntradasDesafio`
  ADD CONSTRAINT `EntradasDesafio_ibfk_1` FOREIGN KEY (`IdDesafio`) REFERENCES `Desafio` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `HistoricoDesafio`
--
ALTER TABLE `HistoricoDesafio`
  ADD CONSTRAINT `historicodesafio_ibfk_1` FOREIGN KEY (`IdResposta`) REFERENCES `Resposta` (`Id`);

--
-- Limitadores para a tabela `Resposta`
--
ALTER TABLE `Resposta`
  ADD CONSTRAINT `Resposta_ibfk_1` FOREIGN KEY (`IdDesafio`) REFERENCES `Desafio` (`Id`),
  ADD CONSTRAINT `Resposta_ibfk_2` FOREIGN KEY (`IdUsuario`) REFERENCES `Usuario` (`Id`);

--
-- Limitadores para a tabela `RespostaTreino`
--
ALTER TABLE `RespostaTreino`
  ADD CONSTRAINT `respostatreino_ibfk_1` FOREIGN KEY (`IdUsuario`) REFERENCES `Usuario` (`Id`),
  ADD CONSTRAINT `respostatreino_ibfk_2` FOREIGN KEY (`IdDesafio`) REFERENCES `Desafio` (`Id`);

--
-- Limitadores para a tabela `SaidasDesafio`
--
ALTER TABLE `SaidasDesafio`
  ADD CONSTRAINT `fk_SaidasDesafio_Desafio1` FOREIGN KEY (`IdDesafio`) REFERENCES `Desafio` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `Usuario`
--
ALTER TABLE `Usuario`
  ADD CONSTRAINT `Usuario_ibfk_1` FOREIGN KEY (`IdImagem`) REFERENCES `Imagem` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
