var express = require('express'),
    nodemailer = require('nodemailer'),
    load = require('express-load'),
    app = express(),
    mysql = require('mysql'),
    session = require('express-session'),
    bodyParser = require('body-parser'),
    http = require('http').Server(app),
    io = require('socket.io').listen(http),
    q = require('q'),
    path = require('path'),
    multer = require('multer'),
    port = process.env.PORT || 3000,
    storage = multer.diskStorage({
        destination: './app/assets/img/fotosUpload/',
        filename: function(req, file, cb) {
            cb(null, file.originalname.replace(path.extname(file.originalname), "") + '-' + Date.now() + path.extname(file.originalname))
        }
    });
var clients = [];
var upload = multer({ storage: storage })

//Local
//var connection = mysql.createConnection("mysql://root@localhost/codelive");
//Remoto:

var connection = mysql.createConnection({
    host: 'fabrica.ulbra-to.br',
    database: 'codelive',
    user: 'jesiel',
    password: 'THE#1code9*'
});

app.set('io', io);
app.set("db", connection);
app.set("q", q);
app.set("upload", upload);
app.set("clientsLogged", clients);
app.set("nodemailer", nodemailer);

io.on('connection', function(socket) {

    socket.on('novoUsuarioLogado', function(data) {
        if (clients.length == 0) {
            clients.push({
                name: data.NomeExibicao,
                conn: socket
            });
        } else {
            if (findClient(data.NomeExibicao) == -1) {
                clients.push({
                    name: data.NomeExibicao,
                    conn: socket
                });
            }
        }
        socket.broadcast.emit('novoUsuarioLogado', data.NomeExibicao + " entrou no jogo!");
        console.log('Nº usuários conectados: ' + clients.length);
    });

    socket.on('disconnect', function() {
        deleteClient(socket);
    });

    socket.on('logout', function() {
        deleteClient(socket);
        console.log('Nº usuários conectados: ' + clients.length);
    });

    function deleteClient(sk) {
        clients.forEach(function(client, i) {
            if (client.conn == sk) {
                clients.splice(i, 1);
            }
        });
    }

    function findClient(nomeExibicao) {
        var aux = [];
        clients.forEach(function(client) {
            aux.push(client.name);
        })
        return aux.indexOf(nomeExibicao);
    }
});

//front-end da aplicação
app.use(express.static(__dirname + "/app"));
//frameworks js e css
app.use(express.static(path.join(__dirname, '/node_modules')));
//converte as requisições para json
app.use(bodyParser.json());
//configuração da sessão
app.use(session({
    secret: 'keyboard cat',
    resave: true,
    saveUninitialized: true,
    cookie: { secure: false }
}));

//mapea o arquivo de rotas e os controllers do back-end
load('models').then('api').then('rotas').into(app);

http.listen(port, function() {
    console.log("Servidor rodando na porta: " + port);
});